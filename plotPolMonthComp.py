#!/usr/bin/env python 
# python script to plot the h/v results and compare 2 months
import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
from mpl_toolkits.basemap import Basemap, addcyclic, cm
from obspy import read_inventory, read
from obspy.geodetics.base import gps2dist_azimuth
from obspy.imaging.beachball import beach
from matplotlib import colors
from scipy.ndimage.filters import minimum_filter, maximum_filter
from netCDF4 import Dataset as NetCDFFile
from calendar import month_name
from mpl_toolkits.axes_grid.inset_locator import InsetPosition
from mpl_toolkits.axes_grid1 import make_axes_locatable
#---------------------------------------------------------------------
# get the min-max values for the sea-pressure levels
def extrema(mat,mode='wrap',window=10):
    """find the indices of local extrema (min and max)
        in the input array."""
    mn = minimum_filter(mat, size=window, mode=mode)
    mx = maximum_filter(mat, size=window, mode=mode)
    # (mat == mx) true if pixel is equal to the local max
    # (mat == mn) true if pixel is equal to the local in
    # Return the indices of the maxima, minima
    return np.nonzero(mat == mn), np.nonzero(mat == mx)

#---------------------------------------------------------------------
# convert between cartesian and polar coordinates
def cart2pol(x, y):
    ''' convert between cartesian to polar coordinates'''
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    ''' convert between polar to catresian coordinates'''
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

#---------------------------------------------------------------------
# compute the lat and lon given a bearing and distance 
def getLatLon(lat0, lon0, theta, d):
    ''' compute the lat and lon from a point given a bearing and azimuth

        Reference: http://www.movable-type.co.uk/scripts/latlong.html
    '''
    R = 6378.1 #Radius of the Earth
    # convert lat0 and lon0 to radian
    phi1 = np.deg2rad(lat0)
    lambda1 = np.deg2rad(lon0)
    theta = np.deg2rad(theta)
    delta = d/R

    # compute the latitude
    phi2 = np.arcsin( np.sin(phi1) * np.cos(delta) + 
            np.cos(phi1) * np.sin(delta) * np.cos(theta))
    
    a = np.sin(theta) * np.sin(delta) * np.cos(phi1)
    b = np.cos(delta) - np.sin(phi1) * np.sin(phi2)

    lambda2 = lambda1 + np.arctan2(a, b)

    lat2 = np.rad2deg(phi2)
    lon2 = np.rad2deg(lambda2)

    return lat2, lon2

#---------------------------------------------------------------------
def main():
    ''' 
    Main script to plot monthly results from the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                                    'Plot the monthly H/V results')
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs=2,
                    help='2 months to process and compare', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--channel', type=str,
                    help='Channel name to get the station info.\n'
                    'Default is BHZ', required=False)
       
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']

    if args['channel'] is None:
        channel='BHZ'
    else:
        channel=args['channel']


    day_sec = 24.*60.*60.
    
    norm_hv = plt.Normalize(0,1)
    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = np.loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            freq_list = list(np.unique(freq))
            # compute the period 
            T = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
 

            # filter the results based on the DOP threshold
            # convert T, time and hv into numpy arrays
            T = np.array(T)
            baz = np.array(azi[dop >= dop_thresh]) 
            freqD = freq[dop >= dop_thresh]

            # make a polar histogram plot 
            fig = plt.figure(1, figsize=(12,7))
            plt.rcParams['xtick.labelsize']=16
            plt.rcParams['ytick.labelsize']=16
            # histogram of the back azimuth
            nBins = 360/10
            hBins = np.arange(-180,190,10) # create bins centred on the values
            axP = plt.subplot(1,2,kk+1, polar=True)
            # setting the axis to be in geographical coordinates
            axP.set_theta_direction(-1)
            axP.set_theta_offset(np.pi/2.0)
            # generate a polar histogram of the back azimuth as a function of frequency
            deltaF = 0.05    
            fBins = np.linspace(np.min(freq_list), np.max(freq_list), 25)
            deltaA = np.deg2rad(5)
            aBins = np.arange(-np.pi, np.pi+deltaA, deltaA) # angular bins
            thetaBin, rBin = np.meshgrid(aBins, fBins)
            bazR = np.deg2rad(baz)

            # compute the 2D histogram 
            [bazFreqH, xbin, ybin] = np.histogram2d(x=freqD, y=bazR, 
                    bins=[fBins, aBins])

            bazFreqMax = bazFreqH.max()
            bazFreqMin = bazFreqH.min()
            bazFreqN = (bazFreqH -bazFreqMin) / (bazFreqMax - bazFreqMin)  
 
            # mask the histograms setting 0 values to nan
#            bazFreqH_mask = np.ma.masked_where(bazFreqH==0, bazFreqH) 
            # normalize the plot based on the maximum  
#            bazFreqMax = bazFreqH_mask.max()
#            bazFreqMin = bazFreqH_mask.min()
#            bazFreqN = (bazFreqH_mask -bazFreqMin) / (bazFreqMax - bazFreqMin)  
            # plot the results
            normC = colors.Normalize(vmin=bazFreqMin, vmax=bazFreqMax)
    
            pc=axP.pcolor(thetaBin, rBin, bazFreqN, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r', norm=norm_hv)
            # mask values below the minimum we have 
            axP.set_ylim([0, 0.5])
            rticks = np.arange(0,0.6,0.1)
            #set the angular tick labels
            thetaticks = np.arange(0,360,45)
            axP.set_thetagrids(thetaticks, frac=1.15) 
            axP.set_yticks(rticks)
            axP.set_yticklabels(rticks, color='black')
            #axPylabel.set_color('grey')
            mn=month_name[int(month)]
            axP.set_title("%s s" %(mn, year), fontsize=18, y=1.1)
            plt.subplots_adjust(left=0.1, wspace=0.3)

    fig.suptitle("%s DOP = %s" %(station, dop_thresh), size=20)
    # sort out the colour bar
    cax = fig.add_axes([0.25, 0.1, 0.5, 0.03])
    smHV = plt.cm.ScalarMappable(cmap=plt.cm.Spectral_r, norm=norm_hv)
    smHV.set_array(bazFreqN)
    cb = plt.colorbar(smHV, cax=cax, orientation='horizontal')
    cb.set_label('Normalised count', fontsize=16)
    cb.set_clim([0, 1])
    cbTicks = np.arange(0,1.1,0.2) 
    cbTickMark = ["%s" %c for c in cbTicks]
    cb.set_ticks(cbTicks)
    cb.set_ticklabels(cbTickMark)
    cb.ax.tick_params(labelsize=16)
    plt.show()    

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
