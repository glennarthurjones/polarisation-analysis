#!/usr/bin/env python 
# Python script to read the monthly summary files and cat them  
import os
import argparse
import datetime
from numpy import sort, savetxt, log10, transpose
from calendar import monthrange, datetime
#---------------------------------------------------------------------
def main():
    ''' Script to summarise the results of the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                "Year summary of the results of H/V analysis.\n"
                "Must have performed monthly summary prior to running command",
                formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    measurement_code = args['code']
    locID = args['locID']
    months = range(1,13)  

    for year in years:
        for loc in locID:
            output_file = ("%s/%s_%s_%s_summary.txt"
                %(station, year,  measurement_code, loc))
            with open(output_file, 'w') as outfile:
                # write the header of the file
                header='# azimuth, frequency, DOP, Day of year, time, H/V\n'
                outfile.write(header)
                for month in months:
                    month = int(month)
                    dirname = "%s/%s.%02d" %(station, year, month)
                    fname = ("%s/%s_%s_summary.txt" 
                        %(dirname, measurement_code, loc))
                    print '--> File to be processed %s' % fname
                    if not os.path.isfile(fname):
                        print '%s does not exist. Exiting script' %fname
                        return
                    with open(fname, 'r') as infile:
                        for line in infile:
                            # check for comments                            
                            if line.startswith('#'):
                                continue
                            outfile.write(line) 
#
#---------------------------------------------------------------------
if __name__=="__main__":
    main()
