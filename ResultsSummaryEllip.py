#!/usr/bin/env python 
# Python script to generate monthly summaries of the Ellipticity analysis
import os
import argparse
import datetime
from numpy import sort, savetxt, log10, transpose
from calendar import monthrange, datetime
#---------------------------------------------------------------------
def main():
    ''' Script to summarise the results of the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                                    'Monthly summary of the results of H/V analysis')
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.8', required=False)
   
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    months = args['months']
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.8
    else:
        dop_thresh = args['dop']
    day_sec = 24.*60.*60.
    
    
    # loop over the years and then months
    for year in years:
        for month in months:
            month = int(month)
            dirname = "%s/%s.%02d" %(station, year, month)
            print dirname
#            dayList = os.listdir(dirname)
#            days = [jj for jj in dayList if jj not in 'EVENTS-INFO']
            nDays = monthrange(int(year),int(month))[1]
            days = ['%02d' % (jj+1) for jj in range(0,nDays)]
            # loop over the station location ID
            for loc in locID:
                # set up the arrays to store the data
                azi, freq, freq_list, dop, time, hv, doy = [],[],[],[],[],[], []
                # loop over the days we have data
                for day in days:
                    day_folder = '%s/%s' %(dirname, day)
                    output_file = ('%s/%s.%02d.%s.%s.%s.asc' 
                            %(day_folder, year, month,day, measurement_code, loc))
                    print '--> File to be processed %s'  % output_file
                    if not os.path.isfile(output_file):
                        continue
                    # compute day of year
                    dt = datetime.datetime.strptime(
                            '%s.%s.%s' %(year, month, int(day)),'%Y.%m.%d')
                    tt = dt.timetuple().tm_yday
                    # read daily files
                    with open(output_file) as fp:
                        for line in fp:
                            if line.split()[0] == year:
                                pass
                            else:
                                azi_tmp = float(line.split()[0])
				freq_tmp = float(line.split()[1])
				dop_tmp = float(line.split()[2])
				time_tmp = float(line.split()[3])
				hv_tmp = log10(float(line.split()[5]))	
				# check to make sure the results are within our DOP
				if dop_tmp >= dop_thresh:
                                    azi.append(azi_tmp)
				    freq.append(freq_tmp)
				    dop.append(dop_tmp)
				    time.append(time_tmp)
				    hv.append(hv_tmp)
                                    doy.append(tt) 
				    # check to see if there are unique frequency values
				    if freq_tmp not in freq_list:
				        freq_list.append(freq_tmp)
                 
                # Save the per month results 
                fnameOut = ('%s/%s_%s_summary.txt' 
                    %(dirname, measurement_code, loc))
                print ' --> Output file %s' % fnameOut
                savetxt(fnameOut, transpose([azi, freq, dop, doy, time, hv]),
                        fmt=['%.4f','%.2f','%.4f','%d','%.4f','%.4f'],
                delimiter=',', comments='#' ,
		header='azimuth, frequency, DOP, Day of year, time, H/V')

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
