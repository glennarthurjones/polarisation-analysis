#!/usr/bin/env python
#
# Python script to pre process the mseed data to cut it to the correct time
# and remove the instrument response using SAC

import numpy as np
from obspy.core import read, UTCDateTime
from obspy.core.inventory import read_inventory
import os
import glob
from tracePreProc import make_same_length, check_and_phase_shift
from tracePreProc import nextpow2, gapCheck, nanCheck, traceRot

def preprocessing(dirname, subsamp=2.0, chunkSize=8192, freqFilt=[0.005, 0.5], 
        Event=False, rot=False, reSamp=False, locListG=[]):
	'''
	PreProcesses mseed data by merging, filtering and cutting the traces 
	to be used in the ambient noise H/V analysis. It is assumed that the 
	data were downloaded using obspyDMT and uses the same directory structure.
	Parts of this preprocessing module which deal with data alignment and gaps 
	are taken from MSNoise 1.5 

	Parameters:
	Input:
		fnameIn   : base directory for the mseed data

		subsamp   : frequency we wish to subsample the trace data.
			    Default = 2

		chunkSize : size in samples of each output file.
			    Default = 8192

                freqFilt  : list of the min and max frequency filter values for
                            the pre-processing
                            Default = [0.005, 0.5]

                Event     : Boolean: decide if processing is done on an
                            event basis. Default=False

                rot       : Boolean: decide if any rotation will be applied
                            to the data post instrument response removal
                            Default=False

                reSamp   :  Boolean: decide to re-sample the data should it 
                            should we find 2 sampling rates on a given data
                            e.g., instrument service. Default=False

                locListG  :  list of station locations we wish to process
                            Default=[] so all locations will be processed

	Output:
		SAC files which have been cut to the appropriate size
		and saved in the processed directroy

        =======================================================================
        Modified 2018-06-05 by Glenn Jones, Swansea University, UCL
            + Script now allows for event based processing

        Modified 2018-08-26 by Glenn Jones, Swansea University, UCL 
            + Chunk any remainder data which isn't an integer of chunkSize so 
              long as the next power of 2 as chunkSize

        Modified 2018-09-06 by Glenn Jones, Swansea University, UCL
            + Some of the data have different sampling rates and so cannot be
              merged correctly. I have included a some options to either take 
              the minimum sampling rate and downsample the other data to match.
              The default is to skip that day of processing

        Modified 2018-10-25 by Glenn Jones, Swansea Univeristy, UCL
            + We may want to process specific site locations as opposed 
              to all available data. There is now an option to include 
              a location description in the input

        Modified 2019-06-14 by Glenn Jones, Swansea Univeristy, UCL
            + Include the option to process event based data.

        Modified 2019-07-09 by Glenn Jones, Swansea University, UCL
            + locList is being updated globally which has some implication 
              should be have data missing form day 1 of the analysis
              Changed the input variable to locListG to account for this

        Modified 2019-08-06 by Glenn Jones, Swansea University, UCL
            + resp_list may not be present even with some partial data. I have
            added a check to see if these exists and if not exit
	'''
	# define some parameters	
	daySec = 60.*60.*24. # seconds in the day
	chunkTime = float(chunkSize) / float(subsamp)
	filter_freq = 0.25*subsamp # frequency filter used for downsampling
        # the H/V code relies on the FFT and computing the next power of 2 
        # to speed it up. Here I compute the next power of 2 for later
        chunkNP2 = nextpow2(chunkSize) 

	# opening up a preprocessing log file 
	outName = dirname + '/preprocessing.log'	
	pplog = open(outName, 'w')

        # some of the downloaded data will have different locations codes
        # so we must process these different stations separatley 
        stList = glob.glob(dirname + "/raw/*[!.sac]") 
        # check to make sure we have some files in the directory
        if not stList:
            pplog.write('ERROR: No files to process\n')   
            pplog.close()
            return # exit

        # find the number of different station locations based on the file name
        locList = []
        if not locListG: 
            for stL in stList:
                # strip out the base directory from the station list
                stL = os.path.basename(stL)
                [net, station, loc, comp] = stL.split(".")
                if loc not in locList:
                    locList.append(loc)
        else:
            locList = locListG

        # loop over the different station locations
        for jj, locs in enumerate(locList): 
            # get the mseed files in the raw data directory to be processed 
	    try: 
                #st = read(dirname + "/raw/*" + locs + "*[!.sac]")	
                st = read(dirname + "/raw/*." + locs + ".*[!.sac]")	
            except Exception  as error:
                print('ERROR: %s' % error)
                pplog.write('ERROR: %s\n' %error)
	        continue 

            stList.sort()
            # I've come across some data examples where the sampling rate
            # has been changes following servicing
            srList = []
            for tr in st:
                sr = tr.stats.sampling_rate
                if sr not in srList:
                    srList.append(sr)
           
            if jj != 0:
                pplog.write('#=============================\n')
            
            pplog.write('Processing traces:\n')
            # check to see if we have multiple sampling rates
            if len(srList) != 1:
                if reSamp is False:
                    pplog.write('ERROR: Different sampling rates on same day\n')
                    continue
#                    pplog.close()
#                    return # exit
                else:
                    # resample the data 
                    goal_sr = np.round(min(srList))
                    # loop over traces and check sampling rates
                    for tr in st:
                        if tr.stats.sampling_rate != goal_sr:
                            pplog.write('\n Traces %s must be downsampled \n' 
                                    %tr.id)
                            dtatype = tr.data.dtype
                            # resample the data using Fourier method 
                            tr.resample(goal_sr,no_filter=False)
                            tr.data = tr.data.astype(dtatype)

            # merge the respective stations and interpolate any overlap values
	    st.merge(method=1, interpolation_samples=3, fill_value=None)
            # get the trace ID and write to file 
            for tr in st:
		pplog.write(tr.id + '  ')	
            
            # plot the raw data
            if Event is False:
                if not os.path.isdir("%s/../Figures/raw" % dirname): 
                        os.makedirs("%s/../Figures/raw" % dirname)
                for tr in st:
                    trID = tr.id
                    fOut = ("%s/../Figures/raw/%s.raw.day%s.png" 
                            % (dirname, trID, dirname.split('/')[2]))
                    tr.plot(type='dayplot', outfile=fOut)
                # combine the traces and plot
                st = st.split()
                fileOut = ("%s.%s.traces.raw.day%s.png"
                    % (str(tr.stats.network), locs, 
                        dirname.split('/')[2]))
                fOut = "%s/../Figures/raw/%s" % (dirname, fileOut)
                st.plot(outfile=fOut)

	    # split the stream by any gaps
	    st=st.split() 	
	    
	    # check to make sure the traces have the same sample points if the 
	    # stream was interupted
	    pplog.write('\nChecking sample alignment \n')	
            taper_length = 20.0 # set taper length in seconds
	    for i, trace in enumerate(st):
                # check to make sure that we have enough data to process and if
                # not we remove the trace
                if trace.stats.npts < 4 * taper_length*trace.stats.sampling_rate:
                    st.remove(trace)
                else:
                    #st[i] = check_and_phase_shift(trace, taper_length)
                    trace = check_and_phase_shift(trace, taper_length)

	    # setting up the start and end times for the data
	    year, month = dirname.split('/')[1].split('.')
	    #================================================
            # Modification 2018-06-05 Glenn Jones Swansea Uni.
            if Event:
                pplog.write('Event processing\n')   
                # load the event data python pickle stored in EVENTS-INFO
                try:
                    eventInfo = np.load(dirname + '/../EVENTS-INFO/event_list_pickle') 
                except Exception  as error:
                    print('ERROR: %s' % error)
                    pplog.write('ERROR: %s\n' %error)
	            continue 
                # loop over the pickle data and match the event_id with the current event 
                starttime=[]
                for eI in eventInfo:
                    if eI['event_id']==os.path.basename(dirname):
                        day = eI['t1'].day
                        starttime = eI['t1']
                        endtime = eI['t2']
                if not starttime:
                    error = 'no start time found for event. Skip processing'
                    print('ERROR: %s' % error)
                    pplog.write('ERROR: %s\n' %error)
	            continue
            else:
                day = dirname.split('/')[2]
	        starttime = UTCDateTime(int(year),int(month), int(day))
	        endtime = starttime + daySec - 1/subsamp

	    pplog.write('Checking gaps \n')
	    # check for gaps in the data and interpolate if the gaps are small 
            st = gapCheck(st)
#	    if len(st.get_gaps())> 0:
#                max_gap = 10
#       		only_too_long = False
#		# loop over the gaps and check to see if we can interpolate over some of them
#		while st.get_gaps() and not only_too_long:
#		    too_long = 0
#		    gaps = st.get_gaps()
#		    # loop over the gaps and see if they are larger than max_gap
#		    for gap in gaps:
#			# interpolate and remove the current gap
#			if int(gap[-1]) <= max_gap:
#			    st[gap[0]] = st[gap[0]].__add__(st[gap[1]], method=1,
#                                    fill_value="interpolate")
#			    st.remove(st[gap[1]])
#			    break
#			else:
#			    too_long += 1
#                 
#		    if too_long == len(gaps):
#		        only_too_long = True

	    # re-split the traces following the checks for gaps
	    st = st.split()

	    pplog.write('Make traces same length \n')
	    # we now check the traces of each component and make sure they 
            # are the same length
	    st = make_same_length(st)
	    # check to make sure the traces have enough samples 	
	    for tr in st:
		r=tr.stats.sampling_rate/subsamp
		if tr.stats.npts < chunkSize*r:
			st.remove(tr)

	    # trim the traces and merge 	
	    st.trim(starttime, endtime, nearest_sample=False)
	    st = st.merge()   
            chanList = [] # store channel names
            traceList = [] # store the number of traces we've split the data into
            # Save the data as sac files
            for tr in st:
                # We need to make sure we save all traces especially if there 
                # are gaps during the day. We split the traces into a stream 
                # to see how many stations we have
                trS = tr.split()
                chanList.append(tr.stats.channel)  
                for kk, trr in enumerate(trS):
                    # save the number of unique traces
                    if ("%02d" %(kk+1)) not in traceList:
                        traceList.append("%02d" %(kk+1))  
                    # SAC transfer function assumes that the trace is 
                    # a multiple of 2 in size
                    npts = trr.stats.npts
                    if np.mod(npts, 2) == 1:
                        trr.data = trr.data[0:-1] # remove a sample
		    # we now save each trace as a SAC files 
		    fout = '%s/raw/%s.tr%02d.sac'   %(dirname,trr.id,kk+1) 
	            trr.data = trr.data.astype(np.float32)	
                    trr.write(fout, format='SAC')

# OLD
	# trim the traces to be the correct size 
#	pplog.write('Subsampling data to %s \n' %subsamp)
#	for tr in st:	
#		tr.interpolate(sampling_rate = subsamp)
#		tr.trim(starttime, endtime)
#
#	# for each trace in the stream we check its size to make sure we 
#	# have enough data to properly remove the instrument response 
#	taper_length = 20.0 # seconds
#	for tr in st:
#		taperChunk = 4 * taper_length * tr.stats.sampling_rate
#		# check to make sure that each trace is as big as the taper or chunk
#		# we wish to cut the data
#		if taperChunk > chunkSize: 
#			if tr.stats.npts < taperChunk:
#				st.remove(tr)
#		elif chunkSize > taperChunk:
#			if tr.stats.npts < chunkSize:
#				st.remove(tr)
#	
#		
#		# we now save each trace as a SAC files 
#		fout = dirname + "/raw/" + tr.id +".sac"
#		tr.write(fout, format='SAC')
#-----------------------------------------------------------------------------
# Cut the pre-processed sac files into chunks for the H/V analysis
#		nChunk = int(np.floor(tr.stats.npts/chunkSize))
#		net = tr.stats.network	
#		sta = tr.stats.station	
#		loc = tr.stats.location
#		chan = tr.stats.channel
#		
#		for ii in range(0,nChunk):
#			stime = tr.stats.starttime + ii * chunkTime
#			etime = stime + chunkTime - 1.0/subsamp
#			trC = tr.coy()
#			trC.trim(stme, etime)	
#
#			# get the name of the output file
#			fout = "%s/raw/%s.%s.%s.%s_chunk_%isps%i.sac" \
#				%(dirname, net,sta, loc, chan, ii, subsamp)
#			# save the results as SAC files to be reopened and processed
#			trC.write(fout, format='SAC')

#-----------------------------------------------------------------------------
# Converting the XML file to pole and zeros and remove the instrument response
# from the SAC data files
	    resp_list = glob.glob(dirname +"/resp/STXML*." + locs + ".*")
            if not resp_list:
                pplog.write('ERROR: NO instrument response files to process\n')   
                pplog.close()
                return # exit
            nResp=0
	    for resp_file in resp_list:
		inv = read_inventory(resp_file, format="STATIONXML")
		net = inv[0].code
		sta = inv[0][0].code
	        loc = inv[0][0][0].location_code	
                chan = inv[0][0][0].code

		fout = (dirname +"/resp/" + net + "." + sta + "." + loc + "." 
                        + chan + "_pz")
		inv.write(fout, format="SACPZ")
                nResp+=1
            # check to make sure we have at 3 channels and associated
            # meta-data before we do any processing
            if len(chanList) != 3:
                errMsg = ('ERROR: Only %d traces available. 3 needed to'\
                        ' process the data' % len(st))
                pplog.write('ERROR: %s\n' %errMsg)
                continue

            if nResp != 3:
                errMsg = ('ERROR Only %d headers available. 3 needed to'\
                        'process the data.' % len(st))
                pplog.write('ERROR: %s\n' %errMsg)
                continue

	    pplog.write('Remove instrument response \n')
            file_list = glob.glob(dirname + "/raw/*." + locs + ".*.sac")		
	    for fl in file_list:
		tr = read(fl)[0]
		sta = tr.stats.station
		net = tr.stats.network
		chan = tr.stats.channel
		loc = tr.stats.location		
                # get the trace number
                flN = os.path.basename(fl)  # get the file name only
                flTr = flN.split('.')[4]
	
                # check to make sure the output directory exists
                if not os.path.isdir("%s/processed" %dirname):
                    os.mkdir("%s/processed" %dirname)

                # defining input for the sac macro 
                inputfile = fl
		pzfile = (dirname + "/resp/" + net + "." + sta + "." 
                        + loc + "." + chan + "_pz")

                # check to make sure we have an instrucment response file 
                if not os.path.isfile(pzfile):
                    er = "%s does not exist" % pzfile 
                    pplog.write('ERROR: %s\n' %er)
                    continue 

		fout = "%s/processed/%s.%s.%s.%s.%s.sac.corr" \
				%(dirname, net,sta, loc, chan, flTr)
                
		# the sampling rate of the data is 2Hz so we use a 
		# an upped filter bound of 0.8-1Hz
                # Currently the macro min and max fequencies are hard wired 
#		macroLine = "sac > macro remove_resp " \
		macroLine = "sac > macro remove_resp_ellip " \
			+ inputfile + " " + pzfile + " " + fout 
                os.system(macroLine)
#-----------------------------------------------------------------------------
# Check to see if we need to rotate the traces and load the instrument files
            if rot: 
                pplog.write('Rotate traces to ZNE \n' )
                inv = read_inventory(resp_list[0], format="STATIONXML")
                for resp_file in resp_list[1:]:
                    inv += read_inventory(resp_file, format="STATIONXML") 

            # Check to see if the traces contain nan or inf and build a 
            # channel list form this to be processed. 
            chanListN = []
            for tL in traceList: 
                fl = "%s/processed/*%s.*tr%s.sac.corr" %(dirname, locs, tL) 
                file_list = glob.glob(fl)
                st = read(file_list[0])
                for fl in file_list[1:]:
                    st += read(fl) 
                # check to see if the traces contain inf or nans
                NanErr = nanCheck(st)
                if NanErr:
                    for nerr in NanErr:
                        errMsg = 'File %s contains nan/inf' %nerr
                        pplog.write('ERROR: %s\n' %errMsg)
                    continue

                if rot: 
                    # get the components of the channels
                    ch = ['%s' %str(tr.stats.channel[-1]) for tr in st]
                    # check to see if more than one of the components 
                    # match with horizontal components E N
                    isec = set(ch).intersection(['E','e','1','N','n','2'])
                    if len(isec) == 2:
                        # use obspy version of trace rotate
                        try:
                            st = traceRot(st, inv)
#                            st.rotate('->ZNE',inventory=inv) # rotate
                        except Exception  as error:
                            print('ERROR: %s' % error)
                            pplog.write('ERROR: %s\n' %error)
                            continue 
                    else:
                        # Error capture
                        if len(st) != 3:
                            errMsg = ('Only %d traces available. 3 needed to'\
                                'rotate data' % len(st))
                            pplog.write('ERROR: %s\n' %errMsg)
                            continue
                    # save the results so we can compare between rotations
                    for tr in st:
                        chan = tr.stats.channel  
                        if chan not in chanListN:
                            chanListN.append(chan)
                        fout = "%s/processed/%s.%s.%s.%s.tr%s.rot.sac.corr" \
                                %(dirname, net, sta, locs, chan, tL)
                        tr.write(fout, format='SAC')
            
                else:
                    for tr in st:
                        chan = tr.stats.channel  
                        if chan not in chanListN:
                            chanListN.append(chan)
# OLD code            
#            if rot: 
#                pplog.write('Rotate traces to ZNE \n' )
#                inv = read_inventory(resp_list[0], format="STATIONXML")
#                for resp_file in resp_list[1:]:
#                    inv += read_inventory(resp_file, format="STATIONXML") 
#           
#                # generate a new channel list which accounts for changes
#                # due to rotation
#                chanListN = []
#                # loop over the trace number 
#                for tL in traceList: 
#                    fl = "%s/processed/*%s.*tr%s.sac.corr" %(dirname, locs, tL) 
#                    file_list = glob.glob(fl)
#                    st = read(file_list[0])
#                    for fl in file_list[1:]:
#                        st += read(fl) 
#                    # check to see if the traces contain inf or nans
#                    NanErr = nanCheck(st)
#                    if NanErr:
#                        for nerr in NanErr:
#                            errMsg = 'File %s contains nan/inf' %nerr
#                            pplog.write('ERROR: %s\n' %errMsg
#                        continue
#
#                    # try and apply the rotation to the data
#                    try:
#                        st.rotate('->ZNE',inventory=inv) # rotate
#                    except Exception  as error:
#                        print('WARNING: %s' % error)
#                        pplog.write('ERROR: %s\n' %error)
#                        continue 
#
#                    # save the results so we can compare between rotations
#                    for tr in st:
#                        chan = tr.stats.channel  
#                        if chan not in chanListN:
#                            chanListN.append(chan)
#                        fout = "%s/processed/%s.%s.%s.%s.tr%s.rot.sac.corr" \
#                                %(dirname, net, sta, locs, chan, tL)
#                        tr.write(fout, format='SAC')
#            else:
#                chanListN = chanList
#-----------------------------------------------------------------------------
# Cut the pre-processed sac files into chunks for the H/V analysis
	    pplog.write('Cut traces to length %s \n' %(chunkSize))
            # loop over the channels
#            for comp in chanList:
            for comp in chanListN:
                if rot:
                    file_list = glob.glob(dirname+"/processed/*" + locs + "." \
                            + comp + "*rot.sac.corr")
                else:
                    file_list = glob.glob(dirname+"/processed/*" + locs + "." \
                            + comp + "*.sac.corr")
                # loop over the files 
                sChunk = 0 # initialize the chunk counter
                for fl in sorted(file_list):
                    tr = read(fl)[0]
		    net = tr.stats.network	
		    sta = tr.stats.station	
		    loc = tr.stats.location
		    chan = tr.stats.channel

		    # downsample and filter the trace
		    tr.detrend(type='demean')
		    tr.detrend(type='linear')
		    tr.taper(max_percentage=None, max_length=1)
		    tr.filter('bandpass', 
                            freqmin=min(freqFilt), freqmax = max(freqFilt), 
                            corners=8, zerophase=True)
	
		    tr.trim(starttime, endtime, nearest_sample=False)
		    tr.interpolate(sampling_rate = subsamp, method="lanczos", a=1)
                    # plot the pre-processed traces before we cut them up
                    if not os.path.isdir("%s/../Figures/processed/%s" 
                            %   (dirname, day)):
                        os.makedirs("%s/../Figures/processed/%s" 
                                % (dirname, day))

                    fl_out = os.path.basename(fl).replace(".sac.corr", 
                            ".day%s.png" %day)
                    fout_plot = ("%s/../Figures/processed/%s/%s" 
                            %(dirname, day, fl_out))
                    tr.plot(type='dayplot',outfile=fout_plot)
                    # plot the complete trace
                    fl_out = fl_out.replace(".png",".trace.png") 
                    fout_plot = ("%s/../Figures/processed/%s/%s" 
                            %(dirname, day, fl_out))
                    tr.plot(outfile=fout_plot)


#       	    dec = int(tr.stats.sampling_rate/subsamp)
#		    tr.decimate(dec, no_filter=True)
		    # get the number of chunks in the file
		    nChunk = int(np.floor(float(tr.stats.npts)/float(chunkSize)))
		    eChunk = sChunk + nChunk 

                    # cut up the data into chunks
#                    for ii in range(sChunk,eChunk):
#                        stime = tr.stats.starttime + ii * chunkTime
                    for q, ii in enumerate(range(sChunk, eChunk)):
                        stime = tr.stats.starttime + q * chunkTime
		        etime = stime + chunkTime - 1.0/subsamp

	    	        trC = tr.copy()
		        trC.trim(stime, etime)	
		        # get the name of the output file
		        fout = "%s/processed/%s.%s.%s.%s_chunk_%isps%i.sac" \
		                %(dirname, net,sta, loc, chan, ii, subsamp)
		        # save the results as SAC files to be reopened and processed
		        trC.write(fout, format='SAC')
                        # clear variable trC
                        trC = None

                    # check to see if the remaining data is long enough 
                    # to be used in the analysis
                    chunkRem = np.remainder(float(tr.stats.npts)/float(chunkSize),
                            nChunk) * chunkSize
                    
                    # compute the next power of 2 for the remainder and compare
                    # with the next power of 2 for the chunk
                    chunkRemNP2 = nextpow2(chunkRem)   
                    if chunkRemNP2 == chunkNP2:
#                        stime = tr.stats.starttime + eChunk *chunkTime
                        # relative start time to the trace
                        stime = tr.stats.starttime + nChunk * chunkTime
                        etime = tr.stats.endtime
                        trC = tr.copy()
                        trC.trim(stime, etime)
                        # get the name of the output file
		        fout = "%s/processed/%s.%s.%s.%s_chunk_%isps%i.sac" \
		            %(dirname, net,sta, loc, chan, eChunk, subsamp)
		        # save the results as SAC files to be reopened and processed
		        trC.write(fout, format='SAC')
                        trC = None
                        # update the end chunk counter
                        eChunk+=1

                    # update the chunk counter
                    sChunk = eChunk
                    tr = None
	    	    # sac trace cleanup

		    fdel = fl			
#		    os.remove(fdel)
	pplog.close()

# Build the base directory 
#data_folder = "CCD"

#daySec = 24.*60.*60
#subsamp=2.0 # subsampling rate of traces
## looping over the directories in the base data folder
#dirList = listdir(data_folder)
#for d in dirList:
#	# list the day files inside d
#	dirname = data_folder + "/%s/" % d
#	year, month = d.split('.')
#	ndays = monthrange(int(year), int(month))
#	daysList = listdir(dirname)
#	days = [jj for jj in daysList if jj not in 'EVENTS-INFO']	
#	for day in days:
#		dirname = data_folder +"/%s/%s" % (d, day)	
#		# obspyDMT generates a processed directory which has
#		# pre-processed mseed (or SAC) data files which were
#		# generated using obspy
#	
#		# remove these data files
#		system("rm " + dirname + "/processed/*")	
#
#		# get the mseed files in the raw data directory to be processed 
#		st = read(dirname + "/raw/*[!.sac]")	
#		# merge the respective stations
#		st.merge()
#		# cut the traces so they span a full day
#		starttime = UTCDateTime(int(year), int(month), int(day))
#		endtime = starttime + daySec - 1.0/subsamp
# 
#		# write out the traces as SAC file
#		for tr in st:
#			fout = dirname + "/raw/" + tr.id +".sac"
#			# the trace now has maksed data object so we need to 
#			# convert this back to a regular array
#			tr.data = tr.data.compressed()
#			# downsample the data from 20Hz to 2Hz so it's the same as 
#			# the CCD_00 dataset
#			tr.interpolate(sampling_rate=subsamp)
#			# trim the data to be a day in size and padd values with 0
#			tr.trim(starttime, endtime, pad=True, fill_value=0)
#			tr.write(fout, format='SAC')
#------------------------------------------------------------------------------
# Converting the XML file to pole and zeros and remove from the SAC data files
#		resp_list = glob.glob(dirname +"/resp/STXML*")
#		for resp_file in resp_list:
#			inv = read_inventory(resp_file, format="STATIONXML")
#			net = inv[0].code
#			sta = inv[0][0].code
#			chan = inv[0][0][0].code
#			fout = dirname +"/resp/" + net + "." + sta + "." + chan + "_pz"
#			inv.write(fout, format="SACPZ")
#
#		file_list = glob.glob(dirname + "/raw/*.sac")		
#		for fl in file_list:
#			tr = read(fl)[0]
#			sta = tr.stats.station
#			net = tr.stats.network
#			chan = tr.stats.channel
#
#			inputfile = fl
#			pzfile = dirname + "/resp/" + net + "." + sta + "." + chan + "_pz"
#			fout = dirname + "/processed/" + net + "." + sta + "." + chan + ".sac.corr"
#			# the sampling rate of the data is 2Hz so we use a 
#			# an upped filter bound of 0.8-1Hz
#			macroLine = "sac > macro remove_resp " \
#				+ inputfile + " " + pzfile + " " + fout 
#			system(macroLine)
