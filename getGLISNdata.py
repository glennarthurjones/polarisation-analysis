#!/usr/bin/env python 
#
# Python script to download mseed data from the GLISN network to perform
# the H/V analysis of Greenland
#
import sys
import os
import argparse
import glob
import re
from calendar import monthrange
from obspy.core import read, UTCDateTime
from shutil import rmtree
#---------------------------------------------------------------------
def makeDict(fname):
    '''
    Function to open the file and generate a dictionary of the relevant variables
    
    Input:
        fname   :   filename to be opened
    Output:
        d       :   variable dictionary
    '''
    d = {}
    with open(fname,'r') as f:
        for line in f:
            if line.rstrip():
                (key, vals) = line.rstrip.split('=')
                d[key] = vals

    return d

#---------------------------------------------------------------------
def dequote(s):
    """
    If a string has single or double quotes around it, remove them.
    Make sure the pair of quotes match.
    If a matching pair of quotes is not found, return the string unchanged.
    """
    if (s[0] == s[-1]) and s.startswith(("'", '"')):
        return s[1:-1]
    return s   

#---------------------------------------------------------------------
def main():
    '''
    Wrapper script around obspDMT to download continuous GLISN data
    '''
    parser = argparse.ArgumentParser(description=
                        "Download GLISN data from IRIS\n"
                        "NOTE: WILDCARDS OPTIONS e.g., '*', 'a*' must be escaped twice \n"
                        "e.g., \"'*'\"  or \\'*'\\",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', nargs='+',
                    help='station name to download', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to download', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to download', required=True)
    parser.add_argument('--loc', type=str,
                    help='station location to download. Wildcards must be escaped twice.\n'
                    'Default is all (*)', 
                    required=False)
    parser.add_argument('--channels', type=str,
                    help='channels to download. Wildcards must be escaped twice.\n'
                    'Default is B*', 
                    required=False)
    parser.add_argument('--network', type=str,
                    help='Network of the station to be downloaded.\n'
                    'Default is _GLISN', 
                    required=False)
    parser.add_argument('--minMag', type=float,
                    help='Minimum magnitude event to download.\n'
                    'Default is download continuous data', 
                    required=False)
    
    # setting up the variables
    args = vars(parser.parse_args())
    stations = args['station']
    years = args['years']
    month_range = args['months'] 
    if len(month_range) > 1:
        month_range.sort(key=int)
    if args['loc'] is None:
        locations="*"
    else:
        locations = args['loc']
        locations = dequote(locations)

    if args['channels'] is None:
        channel = "B*"
    else:
        channel=args['channels']
        channel= dequote(channel)
    
    if args['network'] is None:
        network ="_GLISN"
    else:
        network = args['network']
    
    if args['minMag'] is None:
        minMag = []
    else:
        minMag = args['minMag']

    # hard wired variables
#    network ="_GLISN"
    data_source = "IRIS"
    wave_format = 'sac'
    daySec = 24. * 60. * 60.
# loop over the stations of interest
    for station in stations:    
        data_folder = station  
        tmp_folder = 'temp' # this must be used to download the data
        # loop over the year and month ranges
        for yr in years:
            for m in month_range:
                month = int(m)
                ndays = monthrange(int(yr), int(month))[1]
                max_d= UTCDateTime(int(yr), int(month), ndays) + daySec
                min_date = str(yr) + '-%02d-%02d' %(month, 1)
                max_date = '%s-%02d-%02d' %(max_d.year,max_d.month, max_d.day) 
                
                if not minMag:
                    # set up the obspyDMT command to call
                    line = 'obspyDMT --continuous --datapath %s ' \
            	        '--min_date %s --max_date %s ' \
            	        '--net "%s" --sta "%s" --loc "%s" --cha "%s" '\
                        '--data_source "%s" --waveform_format "%s"'\
            	        % (tmp_folder, min_date, max_date, network, station, 
                                locations,channel, data_source, wave_format)
                    print line
                    # call obspyDMT
                    os.system(line)	
                    # obspyDMT only allows the top directory of the tree to be
                    # computed so we need to generate a folder year.month and 
                    # move all results there
                    datapath = './%s/%s.%02d' %(data_folder, yr, month)
                    if not os.path.isdir(datapath):
                        os.makedirs(datapath)
            	
                    # get a list of the data directories so we can rename them 
                    # to be compatible with the H/V code. This has to be done 
                    # since the trailing day number changes from 1 to 2 digits 
                    # depending on the length of day
                    filelistC = glob.glob(tmp_folder+'/continuous*')
                    for fileIn in filelistC:
                        # strip out the end number
                        d = int(re.findall("\d+", os.path.split(fileIn)[-1])[0])
            	        fileOut = datapath +'/%02d' %d
            	        # rename the folder and place inside datapath
                        try:
            	            os.rename(fileIn, fileOut)
                        except OSError as e:
                            print ("Error: %s - %s." % (e.filename, e.strerror))
                            print ("File: %s - exists" % (fileOut))
                            continue

                    # move the EVENTS-INFO folder
                    eventInfo = './%s/EVENTS-INFO' % tmp_folder
                    try:
                        os.rename(eventInfo,datapath+'/EVENTS-INFO')
                    except OSError as e:
                        print ("Error: %s - %s." % (e.filename, e.strerror))
                        print ("File: %s - exists" % (eventInfo))

                   # cleanup tmp_folder 
                    try:
                        rmtree(tmp_folder)
                    except OSError as e:
                        print ("Error: %s - %s." % (e.filename, e.strerror))

                else:
                    # set some constants for the length of the data download
                    presetT = 100
                    offsetT = 2*60.*60. # 2 hour offset between the event
                    # download the event based dataset
                    line = 'obspyDMT  --datapath %s ' \
        	        '--min_date %s --max_date %s ' \
        	        '--net "%s" --sta "%s" --loc "%s" --cha "%s" '\
                        '--data_source "%s" --event_catalog NEIC_USGS '\
                        '--min_mag %s  --preset %s --offset %s '\
        	        '--waveform_format "%s"'\
        	        % (tmp_folder, min_date, max_date, network, station, 
                            locations, channel, data_source, minMag, presetT, 
                            offsetT, wave_format )
 
                    print line
                    # call osbspyDMT
                    os.system(line)
                    # get a list of the data directories to rename and move them
                    # so they will be compatible with the H/V code
                    datapath = './%s/%s.%02d' %(data_folder, yr, month)
                    if not os.path.isdir(datapath):
                        os.makedirs(datapath)

                    flC	= glob.glob(tmp_folder+'/%d%02d*' 
                            %(int(yr), int(month)))
                    flC.sort()
                    filelistC = [jj for jj in flC if jj not in data_folder+'/EVENTS-INFO']
            
                    
                    datapathEvent = '%s/Event' %(datapath)
                    if not os.path.isdir(datapathEvent):
                        os.mkdir(datapathEvent)
 
                    for ii, fileIn in enumerate(filelistC):
                        # rename the folder based on their event number 
        	        fileOut = datapathEvent + '/' + fileIn.split('/')[1] 
                        # check to make sure the data folder exists and delete 
                        # it if so
                        if os.path.isdir(fileOut):
                            shutil.rmtree(fileOut)
                        # rename the folder and place inside datapath
                        try:
        	            os.rename(fileIn, fileOut)
                        except OSError as e:
                            print ("Error: %s - %s." % (e.filename, e.strerror))
                            print ("File: %s - exists" % (fileOut))
                            continue


                        # move the EVENTS-INFO folder
                        eventInfo = './%s/EVENTS-INFO' % tmp_folder
                        if os.path.isdir(datapathEvent + '/EVENTS-INFO'):
                            shutil.rmtree(datapathEvent + '/EVENTS-INFO')
                        try:
                            os.rename(eventInfo,datapathEvent+'/EVENTS-INFO')
                        except OSError as e:
                            print ("Error: %s - %s." % (e.filename, e.strerror))
                            continue
                    # cleanup tmp_folder 
                    try:
                        rmtree(tmp_folder)
                    except OSError as e:
                        print ("Error: %s - %s." % (e.filename, e.strerror))

        print "-----> Downloaded station %s" %station

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
 
