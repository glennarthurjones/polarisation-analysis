#!/usr/bin/env python 
# Python script to check the log files for any errors in the HV analysis
import os
import argparse
import datetime
from numpy import sort, savetxt, log10, transpose
from calendar import monthrange, datetime
#---------------------------------------------------------------------
def searchfile(fname, keyWords):
    '''
    Search for key words in file
    '''
    try:
        with open(fname, "r") as f:
            searchlines = f.readlines()
        for i, line in enumerate(searchlines):
            for word in keyWords:
                if word in line:
                    print "--> %s found in line %i of file %s\n" %(word, i+1, fname)
    except Exception as err:
        print err


#---------------------------------------------------------------------
def main():
    ''' Script to check log files for any errors in the HV analysis
    '''
    parser = argparse.ArgumentParser(description=
                                    'Check H/V analysis log files for error')
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)

    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']

    keyWords = ['ERROR', 'Error']

    # loop over the years and months
    for year in years:
        for month in months:
            month = int(month)
            dirname = "%s/%s.%02d" %(station, year, month)
            nDays = monthrange(int(year), int(month))[1]
            days = ['%02d' % (jj+1) for jj in range(0, nDays)]
            for loc in locID:
                for day in days:
                    day_folder = '%s/%s' %(dirname, day)
                    print " --> Checking file %s-%02d-%s for errors\n" %(year, month, day)
                    HVlog = ('%s/HV.%s.%s.log' 
                            %(day_folder, measurement_code, loc))
                    polLog = ('%s/polfre.%s.%s.log' 
                            %(day_folder, measurement_code, loc))
                    # open the files and find the key words 
                    searchfile(HVlog, keyWords)
                    searchfile(polLog, keyWords)
                    
#---------------------------------------------------------------------
if __name__=="__main__":
    main()
