# Polarisation analysis

Series of python script to download, pre-process and process seismic ambient 
noise data to obtain their polarisation and H/V properties

# Dependencies

**Python libraries**
+ numpy
+ scipy
+ matplotlib
+ obspy
+ [basemap](https://matplotlib.org/basemap/)
+ netCDF4

**Stand alone code**
+ [Seismic Analysis Code (SAC) version 101.6a](http://ds.iris.edu/ds/nodes/dmc/manuals/sac/)
+ [obspyDMT](https://github.com/kasra-hosseini/obspyDMT)
+ [Time-Frequency Dependent Polarization](http://diapiro.ictja.csic.es/gt/mschi/SCIENCE/tseries.html#software)
+ [Sea pressure data](https://www.esrl.noaa.gov/psd/data/composites/day/)

# Example processing flow

Download the data

```
python getGLISNdata.py --station DY2G --years 2012 --months 1 2 3 --channel '"B*"' --loc '"*"'
```

Preprocessing the data using a combination of obspy and SAC macros

```
python preprocessGLISN.py --station DY2G --year 2012 --months 1 2 3 --freqFilt 0.01 0.5
```

QC the data before processing. The code will generate a GUI which can then be used
to flag days where the data may have issues and will generate a file called exclude.txt
This file can then be used during the main H/V processing to exclude any poor data

```
python dataQC.py --station DY2G --year 2012 --months 1 2 3 --locID 02
```

If we are unhappy with any of the preprocessing we can cleanup using the follwing command.
The user will be prompted by the scrip to enter the station name, year and months to be
removed

```
python cleanupHVsac.py
```

Process the data using Time-Frequency Dependent Polarization analysis. The --exclude option
allows for an exclude file to be included. The --nodes option allows for the processing
to be divided accross a number of different nodes if the user has a multi-core processror.
Care should be taken wth the --nodes option to makes sure the user has sufficient memory
to load the data accross multiple nodes

```
python measure_HV.py --station DY2G --years 2012 --months 1 2 3 --code pol_mp --exclude "exclude.txt" --nodes 4"
```

Check to see if we have any errors from the processing

```
python logError_HV.py --station DY2G --years 2012 --months 1 2 4 --code pol_mp --locID 02
```

Merge the results into monthly and yearly summaries. Note that the yearly summary is 
dependednt on having the monthy examples completed 

```
python ResultsSummaryEllip.py --station DY2G --years 2012 --months 1 2 3 --code pol_mp --locID 02 --dop 0.75
python ResultsSummaryEllipYear.py --stations DY2G --years 2012 --code pol_mp --locID 02 --dop 0.75
```

Summary plots of the polarisation results 

```
python plotPolYear.py --station DY2G --years 2012 --code pol_mp --locID 02
python plotStatMonth.py --station DY2G --years 2012 --months 1 2 3 --code pol_mp --locID 02 --dop 0.95
python plotPolMonth.py --station DY2G --year 2012 --months 1 2 3 --code pol_mp --locID ''
```









