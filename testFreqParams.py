#!/usr/bin/env python
#
# I've  been having some issues in getting the correct frequency bands for the 
# ellipticity analysis so I'm using this script to look into how the H/V
# code computes all of this
import numpy as np
import sys

'''
Script to give the actual frequency bands which will be used by the 
H/V analysis code for a given input frequency band
'''

#---------------------------------------------------------------------
def nextpow2(npts):
    """
    Compute the number of samples in next power of 2 npts

    Input:
        npts: number of points in the seismic trace

    Outputs:
        np2: next power of 2 samples from npts
    """
    if npts > 0:
        np2 = pow(2, np.ceil(np.log(npts)/np.log(2)))
    else:
        np2=0

    return int(np2)

#---------------------------------------------------------------------
f1 = raw_input("Please input lower frequency - f1: ")
try: 
    f1 = float(f1)
except ValueError:
    print "f1 is not a number: Please input a number"
    sys.exit()

f2 = raw_input("Please input upper frequency - f2: ")
try: 
    f2 = float(f2)
except ValueError:
    print "f2 is not a number: Please input a number"
    sys.exit()


# check to make sure f1 is less than f2
if f1 > f2:
    tmp1 = f1
    tmp2 = f2
    f1 = tmp2
    f2 = tmp1
print ("--> f1 = %f f2=%f" %(f1, f2))
#f1=0.0101
#f2=0.498
f1o=f1
f2o=f2
nfreq = 150
nfreqo=nfreq
maxfr = 1021
dt=0.5
fnyq=0.5/dt
# H/V analysis 0 pads the traces to next power of 2 
hourSamp = (60.*60.)/dt
nsamp = nextpow2(hourSamp) 
#nsamp= 7200

df = 1./(float(nsamp)*dt)
f1 = int(f1/df)*df
f2 = int(f2/df)*df
dfstep = (f2-f1)/float(nfreq-1)
if dfstep < df:
    dfstep=df

ndfstep=int(dfstep/df)
dfstep=ndfstep*df
nfreq = int((f2-f1)/dfstep) + 2
f2=(nfreq-1)*dfstep+f1

if nfreq > maxfr:
    nfreq=maxfr
    f2 = (nfreq -1) *dfstep+f1

print 'f1', f1, ' f2', f2, 'nfreq', nfreq



