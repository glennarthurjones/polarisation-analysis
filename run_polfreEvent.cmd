#!/bin/sh
# 
# (1) Edit file names and parameters 
# (2) Execute "run_polfreEQ.cmd" 
# 
# schimmel@ictja.csic.es
#==============================================#
#
#################################
#   PROVIDE 3-C data (SAC files): 
#################################
Z=Z.sac
N=N.sac
E=E.sac

##############################
#   SET PARAMETERS FOR POLFRE:
##############################
##### DOP power:
pow=3     # 3
##### freq. dependent stability window for DOP:
wlen=17     # 17
##### minimum DOP:
dopm=0.75   # 0.75
##### Gauss window for ST: cycle*T=2*std
cycle=3  #1
##### frequency range:
f1=0.015   #0.015
f2=0.33    #0.033	

##### neighbouring frequencies to average: 2+nflen+1:
nflen=2     #2
##### numb. of frequencies in band f1-f2:
nfr=120   #120
##### max number samples to process:
nsp=7200
##### average spectral matrix rather than spectra (keep as it is):
ave=ave
 
par1=" wlenf="$wlen" pow="$pow" "$ave" dopm="$dopm" nsp="$nsp" "
par2=" f1="$f1" f2="$f2" nflen="$nflen" cycle="$cycle" nfr="$nfr""

#############
# EXECUTE PG
#############
\rm -f azi_dopm.asc
\rm -f out_logfile
#polfre_s1.69el $Z $N $E $par1 $par2 hv wdeg=10 zdeg=10 > out_logfile
polfre_s1.69el $Z $N $E $par1 $par2 hv wdeg=10 zdeg=10 > out_logfile 2>&1

if [ -e azi_dopm.asc ];then
    cat azi_dopm.asc >> output_file.asc
else
    echo "-------------------------------------------------------" >> out_logfile
    echo "ERROR file azi_dopm.asc does not exist" >> out_logfile 
    echo "-------------------------------------------------------" >> out_logfile
fi

cat out_logfile >> polfre.log 
exit


