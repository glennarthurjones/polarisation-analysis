#!/usr/bin/env python
#
# Python script to pre-process the GLISN data
#
import sys
import numpy as np
import argparse
from preprocessingHV import preprocessing
from os import listdir
from calendar import monthrange
from glob import glob

import matplotlib
matplotlib.use('Agg')


def main():
    ''' 
    Script to pre-process GLISN network data. The routine assumes that 
    the data were downloaded using obspyDMT using the routine getGLISNdata.py
    '''
    parser = argparse.ArgumentParser(description=
                        'Preprocess GLISN data',
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', nargs='+',
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--subsamp', nargs=1, type=float,
                    help='subsampling rate in samples/sec. Default=2')
    parser.add_argument('--freqFilt', nargs=2, type=float,
                    help='frequency filter range. Default=[0.004, 0.5]')
    parser.add_argument('--chunkSize', nargs=1, type=int,
                    help='size of each chunk of data in samples. Default = 60*60*subsamp')
    parser.add_argument('--reSamp', nargs=1, type=bool,
                    help='re-sample the data should we encounter days with different\n'
                    'sampling rates. Default = False')
    parser.add_argument('--locList', nargs='+',
                    help='station location code to be processed.\n'
                    'Default is to process all location') 
    parser.add_argument('--rot', type=bool,
                    help='option to rotate the traces based on the downloaded headers\n'
                    'Default = False')
    parser.add_argument('--Event', type=bool,
                    help='option to process event based data. Default'
                    'Default is to process continuous data. Default = False')


    # read arguments
    args=vars(parser.parse_args())
    station_folder = args['station']
    years = args['years']
    months = args['months']
    # set up optional arguments
    if args['subsamp'] is None:
        subsamp=2.0
    else:
        subsamp = args['subsamp'][0]

    if args['freqFilt'] is None:
        freqMax = 0.5
        freqMin = 0.004
    else:
        freqMax = max(args['freqFilt'])
        freqMin = min(args['freqFilt'])

    if args['chunkSize'] is None:
        chunkSize = 60 * 60 * subsamp
    else:
        chunkSize = args['chunkSize'][0]
   
    if args['reSamp'] is None:
        reSamp = False
    else:
        reSamp=True
    
    if args['locList'] is None:
        locListG = []
    else:
        locListG = args['locList']

    if args['rot'] is None:
        rot = False
    else:
        rot = True 

    if args['Event'] is None:
        event = False
    else:
        event = True 


#    years = [2012]
#    months = [2]
#    station_folder = ["DY2G"]
#    subsamp = 2.0
#    freqMin = 0.004
#    freqMax = 0.5
## 1 hour trace chunks
#    chunkSize = 60 * 60 * subsamp  
## we will loop over each of these to get the correct base directory
    for jj, data_folder in enumerate(station_folder): 
        if jj != 0:
            print('#=============================')
    
        print '---- Processing  station %s ----\n' % data_folder
        for year in years:
            for month in months:
                base_dir = data_folder + "/%s.%02d" %(year, int(month))
    	        nDays = monthrange(int(year), int(month))[1]
                days = ['%02d' % (jj+1) for jj in range(0,nDays)]
                if not event:
                    for day in days:
        	        dirname = "%s/%s" % (base_dir, day)
    	                # pre process these data 
#    	                preprocessing(dirname, subsamp=subsamp, 
#                            chunkSize=chunkSize,freqFilt=[0.004, 0.5])
    	                preprocessing(dirname, subsamp=subsamp, 
                            chunkSize=chunkSize,freqFilt=[freqMin, freqMax],
                            reSamp=reSamp, locListG=locListG, rot=rot)
   	                print ('--> Processed day %s of month %02d' 
                                %(day, int(month)))
                else:
                    # get a list of the event data
                    events = glob('%s/Event/%s%02d*' 
                        % (base_dir, year, int(month)))
                   
                    if events:
                        for event in events:
                            preprocessing(event, subsamp=subsamp, 
                                chunkSize=chunkSize,freqFilt=[freqMin, freqMax],
                                reSamp=reSamp, locListG=locListG, rot=rot, 
                                Event=True)
   	                    print ('--> Processed event %s of %s month %s' 
                                %(event, month,year))
                    else:
                        print ('--> No event on %s month %s to process' 
                                %(month,year))



                
#---------------------------------------------------------------------
if __name__=="__main__":
    main()


# OLD
#    dirList = listdir(data_folder)
#    for d in dirList:
#    	dirname = data_folder + "/%s/" %d
#    	# list the day files inside
#    	year, month = d.split('.')
#    	ndays = monthrange(int(year), int(month))[1]
#    	daysList = listdir(dirname)
#    	days = [jj for jj in daysList if jj not in 'EVENTS-INFO']
#    	for day in days:
##	for day in range(25,26):
#	    dirname = data_folder + "/%s/%s" % (d, day)
##    	    dirname = data_folder + "/%s/%02d" %(d, day)
#    	    # pre process these data 
#    	    preprocessing(dirname, subsamp=subsamp, chunkSize=chunkSize,
#               freqFilt=[0.004, 0.5])
#    	    print '--> Processed day %s of %s month ' %(day, month)

