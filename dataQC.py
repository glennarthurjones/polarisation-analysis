#!/usr/bin/env python
# Python GUI to be used after the data pre-processing for QC before H/V
import Tkinter as tk
import tkFont
import glob 
import argparse
from os import remove
from PIL import ImageTk, Image 
from calendar import monthrange
from sys import exit
#---------------------------------------------------------------------
# define the GUI class
class QCwindow(tk.Frame):
    def __init__(self,parent):
        self.parent=parent
        tk.Frame.__init__(self)
        self.init_window()

    #==========================================================================
    def update_image(self, move=0):
        """
        move = +1 jumps to the next image
        move = -1 skips back to the previous image
        """
        self.img_idx += move
        fname = IMAGES[self.img_idx%len(IMAGES)]
        # We also want to store the days as a class to be 
        # stored later
        #day = fname.split('.')[5].split('day')[1]
        day = fname.split('day')[1].split('.')[0]
        self.day = day
        # generate the image and make sure a reference is
        # kept in the form of a class attribute
        self.img = ImageTk.PhotoImage(file=fname)

        # set the label image
        self.qc_lbl.configure(image=self.img)
        
        # reset the checkbox to the one associated with this image
        self.value.set(self.img_cbox[self.img_idx%len(IMAGES)])

    #==========================================================================
    def update_checkbox(self):
        idx = self.img_idx%len(CHECKBOX)
        self.img_cbox[idx] = self.value.get()
        # we also want to update the days which will be excluded from the 
        # analysis
        if self.value.get()==1:
            # check to see if this day exists the day exclude file and add if 
            # it's not
            if self.day not in self.ex_lst:
                self.ex_lst.append(self.day)
        else:
            # we now check to see if the day is included in the exclude file
            # and remove it if it is
            if self.day in self.ex_lst:
                self.ex_lst.remove(self.day)
        # save to file
        self.save_exclude()
    #==========================================================================
    def init_window(self):
        self.img_idx = 0
        # set the default checkbox variables
        self.img_cbox = CHECKBOX
        self.value = tk.IntVar()
#        self.ex_lst = []
        self.ex_lst = missing_days()
        print self.ex_lst
        # select a default image
        self.img = ImageTk.PhotoImage(file=DEFAULT_IMAGE)
        #self.day = DEFAULT_IMAGE.split('.')[5].split('day')[1]
        self.day = DEFAULT_IMAGE.split('day')[1].split('.')[0]
        # this is the QC label which contains the image
        self.qc_lbl = tk.Label(self, image=self.img)
        self.qc_lbl.pack(anchor=tk.N, side=tk.TOP, fill=tk.BOTH, expand=1)

        # The next and back buttons. These use lambdas to generate partial
        # functions to call update_image
        self.next_btn = tk.Button(self,text="Next", 
                command=lambda:self.update_image(+1))
        self.next_btn.pack(anchor=tk.S, side=tk.RIGHT)

        self.back_btn = tk.Button(self,text="Back", 
                command=lambda:self.update_image(-1))
        self.back_btn.pack(anchor=tk.S, side=tk.LEFT)
       
        # Set up the tick box which will define the QC tick box
        self.reject_btn = tk.Checkbutton(self, text="Reject",
                command=lambda:self.update_checkbox(),
                variable=self.value) 
        self.reject_btn.pack(anchor=tk.S, side=tk.RIGHT)


    #==========================================================================
    def save_exclude(self):
        if self.ex_lst:
            f = open(FNAME,'w+')
#            for line in self.ex_lst:
            for line in sorted(self.ex_lst):
                f.write('%s \n' %line)
            f.close()

#==========================================================================
def missing_days():
    # We may be missing days from the month and so we need to account
    # for this in our output file
    year = FNAME.split('/')[1].split('.')[0] 
    month = FNAME.split('/')[1].split('.')[1] 
    nDays = monthrange(int(year), int(month))[1]
    img_days = [int(l.split('day')[1].split('.png')[0]) for l in IMAGES]
    month_days = range(1,nDays+1)
    noDta_days = list(set(month_days).difference (img_days))
    # If we don't have any data we need to save these days 
    ex_lst=[]
    if noDta_days:
        noDd = ['%02d' %x for x in noDta_days]
        ex_lst = set(noDd) # remove any duplicate results
        ex_lst=list(ex_lst)
        # This function is called on initiation so will need to be 
        # saved here to makes sure we capture these missing data.
        f = open(FNAME, 'w+')
        for line in sorted(ex_lst):
            f.write('%s \n' %line)
        f.close()
    
    return sorted(ex_lst)

#---------------------------------------------------------------------
def main():
    '''
    GUI to be used after pre-processing data to determine if a given day
    of data should be rejected form the H/V analysis. This is done on 
    a single month by month basis
    '''
    parser = argparse.ArgumentParser(description=
                        "QC data for H/V",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', nargs=1,
                    help="station name to process", required=True)
    parser.add_argument('--year', nargs=1,
                    help="year to process", required=True )
    parser.add_argument('--months', nargs='+',
                    help="months to process", required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    # define global variables
    global IMAGES, DEFAULT_IMAGE, CHECKBOX, FNAME
    # read the input arguments
    args=vars(parser.parse_args())
    station = args['station'][0]
    year = args['year'][0]
    month_range = args['months']
    locID = args['locID']
    
    # loop over the months
    for month in month_range:
        for loc in locID:
            base_dir = "%s/%s.%02d/" %(station, int(year), int(month)) 
            imgfile = (("%sFigures/raw/*.%s.traces.raw*.png") 
                % (base_dir, loc))
            
            # define the output file as a global variable
            FNAME = (("%sexclude.%s.txt") 
                %(base_dir, loc))
            # check to see if the exclude file already exists
            if glob.glob(FNAME):
                wrn = (('WARNING: %s file already exists.')
                    %(FNAME))
                print "--> %s" %wrn
                yes = {'yes','y'}
                no = {'no','n'}
                choice=raw_input("\nDo you want to overwrite the file?: [y/n] ").lower()
                if choice in yes:
                    remove(FNAME)
                elif choice in no: 
                    pass
                else:
                    print("Please respond with [y/n]. Exiting QC")
                    exit()

            # check to see if we have any images
            if not glob.glob(imgfile):
                err = ('ERROR: No data to QC. Check if data is availabe\n'
                        '    %s will be written for all days of the month' 
                        %FNAME)
                print "--> %s" %err
                nDays = monthrange(int(year), int(month))[1]
                f = open(FNAME, 'w+')
                for jj in range(0, nDays): 
                    f.write('%02d \n' %(jj+1))
                f.close()
                # terminate the script
                exit()

            # give values to the global variables
            IMAGES = sorted(glob.glob(imgfile), key=lambda x: x.split('day')[1])
            DEFAULT_IMAGE = IMAGES[0]
            CHECKBOX = [0] * len(IMAGES)

            # define the loading of the GUI
            root = tk.Tk()
            qc = QCwindow(root)
            qc.pack()
            root.mainloop()

#            # We may be missing days from the month and so we need to account
#            # for this in our output file
#            nDays = monthrange(int(year), int(month))[1]
#            img_days = [int(l.split('day')[1].split('.png')[0]) for l in IMAGES]
#            month_days = range(1,nDays+1)
#            noDta_days = list(set(month_days).difference (img_days))
#            # If we don't have any data we need to save these days 
#            if noDta_days:
#                noDd = ['%02d' %x for x in noDta_days]
#                ex_lst = set(noDd)
#                # check to see if we have generated an output file
#                with open(FNAME, 'r+') as f:
#                    for l in f:
#                        ex_lst.add(l.strip())
#                    
#                    ex_lst=list(ex_lst)
#                    # move to start of the file
#                    f.seek(0)
#                    f.truncate()
#                    for line in sorted(ex_lst):
#                        f.write('%s \n' %line)
#                f.close()
#
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

