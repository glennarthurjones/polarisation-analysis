#!/usr/bin/env python
# 
# Python script to take the calculated H/V measurements and 
# generate historgrams of values

import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
from calendar import monthrange, month_name
from matplotlib import colors
#---------------------------------------------------------------------
def aveHV(freq_list, freq, hv, minObs):
    '''
    Estimate the average properties of the HV measurements as a 
    function of period
    '''
    hv_mean, hv_std, hv_median, hv_err_pc, T_list, Nobs\
            = [], [], [], [], [], []
    hv = np.array(hv)
    for f in sorted(freq_list, reverse=True):
        T = 1./f # compute the period
	idx=[i for i,val in enumerate(freq) if val==f]
        if idx:
	    idx=np.array(idx)
        else:
            continue
        
        if len(idx) > minObs:
            # now do the calculations	
            hv_sub = hv[idx]
            hv_mean_tmp = np.mean(hv_sub)
            hv_std_tmp = np.std(hv_sub)
            hv_median_tmp = np.median(hv_sub)
            q1, q2 = np.percentile(hv_sub,[84.1, 15.9])
            err_inf = abs(q2 - hv_median_tmp)
            err_sup = abs(q1 - hv_median_tmp)
            iqr_err = (err_inf + err_sup)/2.0
            
            hv_mean.append(hv_mean_tmp)
            hv_std.append(hv_std_tmp)
            hv_median.append(hv_median_tmp)
            hv_err_pc.append(iqr_err)
            T_list.append(T)
            Nobs.append(len(idx))

    return T_list, hv_mean, hv_std, hv_median, hv_err_pc, Nobs

#---------------------------------------------------------------------
def getAziStat(freq_list, freq, hv, azi, minObs):
    '''
    Take azimuthal bins of 45 degrees and compute the stats 
    in each. The stats will be computed for 50% overlap between bins
    '''
    binStats={}
    wLen = 45.
    bins = np.arange(0, 360, wLen/2)
    ii = len(bins)
    aziM = flipBaz(azi)
    # loop over the bins and compute the stats
    for q, ang in enumerate(bins):
        angMin = np.mod(ang-wLen/2, 360)
        angMax= np.mod(ang+wLen/2, 360)
        # filter the frequency and H/V values
        if angMin > angMax:
            idx = [i for i, v in enumerate(aziM) if v >= angMin or v < angMax]
        else:
            idx = [i for i, v in enumerate(aziM) if v >= angMin and v < angMax]
        # make sure we dont have an empty list
        if idx:
	    idx=np.array(idx)
        else:
            binId = "%s" % ang 
            binStats[binId] = []
            continue
        
        freqF = freq[idx]
        hvF = hv[idx]
        # compute the average results
        tt, hv_mean, hv_std, hv_median, hv_err, nObs = aveHV(
                freq_list, freqF, hvF, minObs)
        binId = "%s" % ang
        binStats[binId] = zip(tt, hv_median, hv_err, nObs) 

    return binStats, bins

#---------------------------------------------------------------------
def MinMaxObs(obs):
    '''
    Get the minimum and maximum number of observations so we can build and
    appropriate colour scale for the whole plot
    '''
    minObs = 1e10 
    maxObs = -1e10
    for x in obs.keys():
        # check to see if it's empty 
        dta = np.array(obs[x])
        if len(dta)==0:
            continue

        if min(dta[:,3]) < minObs:
            minObs = np.min(dta[:,3])
        if max(dta[:,3]) > maxObs:
            maxObs = max(dta[:,3])

    return minObs, maxObs

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 


#---------------------------------------------------------------------
def main():
    '''
    Script to take the polarization results from script measure.py
    and compute the H/V ratio as a fuction of period and the associated
    error
    '''

    parser = argparse.ArgumentParser(description=
                "Compute the H/V ratio from measure_HV.py and plot the results\n"
                "It is assumed that a synthetic dataset has been computed for the\n"
                "station using litho1.0 located in the directory ./station/Inversion/\n"
                "and named station.predicted_data.txt",
                formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--minObs', type=int,
                    help='Minimum number of observations in frequency range to compute \n'
                    'H/Vstats. Default is to use all available observations', 
                    required=False)

    # setting up the variables
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']
    if args['minObs'] is None:
        minObs = 0 
    else:
        minObs = args['minObs']
    
    day_sec = 24.*60.*60.
    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = np.loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            print '--> Processing file %s' %fnameIn
            freq_list = list(np.unique(freq))
            # compute the period 
            T = []
            T_list = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)

            T_list = 1./np.array(freq_list)
            
            #=====================================================
            # Filter the results based on DOP and period
            # filter the frequency and hv results based on dop
            freqD = freq[dop >= dop_thresh]
            hvD = hv[dop >= dop_thresh]
            aziD = azi[dop >= dop_thresh]
            TD = 1./freqD
            # filter the periods and the frequency based on periods < 9s
            freqDT = freqD[TD <9]
            hvDT = hvD[TD<9]
            aziDT = aziD[TD<9]
            TDT = TD[TD<9]
            T_listT = T_list[T_list < 9]
            freq_listT = [freq_list[l] for l,val in enumerate(T_list) if val<9]
            # get the statistsics for the current month 
            tt, hv_mean, hv_std, hv_median, hv_err, nobs = aveHV(
                    freq_listT, freqDT, hvDT, minObs)

            # set up the figure
            fig = plt.figure(2*kk, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            axP = plt.subplot(211)

            # periods end up in a base 10log scale which doesn't work well
            # for 2D histograms so we'll stick with frequency 
            deltaF=0.01
            # get the bin edges for the plot
            fBins = np.arange(np.min(freqDT)-deltaF/2, 
                    np.max(freqDT)+deltaF,deltaF)
            hv_bins = np.linspace(-1.0,1.0,41)
            # Compute the bin centres for labels later
            fBinsC = np.arange(min(freqDT), max(freqDT)+deltaF, deltaF) 
#            fBinsC = np.arange(min(fBins) + deltaF/2, max(fBins), deltaF)
            deltaHV = (max(hv_bins) - min(hv_bins))/(len(hv_bins)-1) 
            hv_binsC = np.arange(min(hv_bins) + deltaHV/2, max(hv_bins), deltaHV)
            
#            HVbins, Fbin = np.meshgrid(hv_binsC, fBinsC)
            HVbins, Fbin = np.meshgrid(hv_bins, fBins)
            # compute the 2D histogram
            [HVT, xbin, ybin] = np.histogram2d(x=hvDT , y=freqDT, bins=[hv_bins, fBins])
            
            # normalize the histogram for each period/frequency
            HVT_norm = HVT / HVT.sum(axis=0)
            normC = colors.Normalize(vmin=0, vmax=0.2)
            pc=axP.pcolor(Fbin, HVbins, HVT_norm.T,
                    edgecolors='w', linewidth=0.25,
                    cmap='Spectral_r', norm=normC)

            # plot the median location and associated error bars on the plot
#            plt.errorbar(sorted(freq_listT, reverse=True), hv_median, yerr=hv_err, 
#                    fmt=" ", color="k", linewidth=2)
#            plt.plot(sorted(freq_listT, reverse=True),hv_median,
#                    '.' ,color="k", markersize=8, markeredgecolor='k')
            plt.errorbar(1./np.array(tt), hv_median, yerr=hv_err, 
                    fmt=" ", color="k", linewidth=2)
            plt.plot(1./np.array(tt),hv_median,
                    '.' ,color="k", markersize=8, markeredgecolor='k')


            xTicks = np.array(freq_listT[::5])
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks, xTickMark)
            plt.xlabel('Frequency',fontsize=20)
            plt.xlim(min(fBins),max(fBins))
            plt.ylabel('log(H/V)',fontsize=20)

            cbar = fig.colorbar(pc, ticks=np.arange(0,0.21,0.05)) 
            cbar.set_label('Normalised count',fontsize=18)

            # add a plot of H/V vs period to compare with the results of
            # litho1.0
            fnameSyn = "%s/Inversion/%s.predicted_data.txt" % (station, station)
            # check to see if we have a synthetic example from litho1.0
            plt.subplot(211)
            try:
                tmp = np.loadtxt(fnameSyn)
            except Exception as error:
                print('WARNING: %s' %error)
                mn=month_name[int(month)]
                fig.suptitle("%s %s \n DOP = %s" %(mn, year, dop_thresh), size=22)
                plt.show()
                continue

            plt.subplot(212)
            # read the results from the forward modelling of litho1.0
            tmp = np.loadtxt(fnameSyn)
            Tm = tmp[:,0]
            Em = tmp[:,1]

            plt.errorbar(tt, hv_median, yerr=hv_err, 
                fmt=" ", color="red", linewidth=2)
            plt.semilogx(tt,hv_median,
                linestyle='-' ,color="red", label="Observed")

            plt.semilogx(Tm[Tm<9], Em[Tm<9], color="blue",
                linestyle='-', linewidth=2, label="litho1.0")
            plt.plot(tt, hv_median,'.' ,color="r", 
                    markersize=9, markeredgecolor='r')

            plt.xlabel("Period (s)", fontsize=20)
            plt.ylabel("log(H/V)", fontsize=20)
            plt.legend(fontsize=16, scatterpoints=1,loc='lower right')
            plt.ylim(-1,1)
            xTicks = np.arange(2,11,2)
#            xTicks = np.arange(0,101,10)
            xTicks[0]=2
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks, xTickMark)
            plt.xlim(1.9,10)
#            plt.xlim(1.9,100)
            plt.grid()
            
            mn=month_name[int(month)]
            fig.suptitle("%s %s \n DOP = %s" %(mn, year, dop_thresh), size=22)
            #========================================

            HVbinStats, bins = getAziStat(freq_listT, freqDT, hvDT, 
                    aziDT, minObs)
            pMarkers = ["o","v","^", "<", ">", "s", "p", "|", 
                    "*", "+", "x", "d", "H", ".", "1", "2"]

            fig = plt.figure(2*kk+1, figsize=(12,9))
            ax = fig.add_axes([0.1, 0.15, 0.65, 0.7])
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            plt.plot(tt, hv_median,'-' ,color="black", 
                linewidth=2, alpha=0.8,
                label="360$^\circ$ average")
            # set up the colorbar values
            minO, maxO = MinMaxObs(HVbinStats)
            normCol = colors.LogNorm(vmin=minO, vmax=maxO)
            for kk, ang in enumerate(bins):
                stats = np.array(HVbinStats[("%s" %ang)])
                if len(stats)==0: 
                    continue

                pcs=ax.scatter(stats[:,0], stats[:,1], c=stats[:,3], 
                    marker=pMarkers[kk], edgecolors=None, s=100, alpha=0.8,
                    label=("%s$^\circ$" %ang), norm=normCol, cmap='viridis')
            
                ax.legend(bbox_to_anchor=(1.05, 1), fontsize=16, scatterpoints=1,
                        loc='upper left',borderaxespad=0.,
                        framealpha=0.5, fancybox=True)
 
            plt.xlabel("Period (s)", fontsize=20)
            plt.ylabel("log(H/V)", fontsize=20)
#            plt.legend(fontsize=16, scatterpoints=1,loc='lower right',
#                    framealpha=0.5, fancybox=True)
            plt.ylim(-1,1)
            xTicks = np.arange(2,11,2)
            xTicks[0]=2
            xTickMark = ["%s" % x for x in xTicks]
            plt.xlim(1.9,9)
            plt.grid()
            
            nearest10= np.floor(maxO/(np.log10(maxO)))
            maxVal = nearest10*10**np.floor(np.log10(maxO))
            cTicks = [10**x  for x in range(0,int(np.floor(np.log10(maxO)+1)))]
            cbar = fig.colorbar(pcs, ticks=cTicks, orientation='horizontal')
            cbar.set_label('Number of observations',fontsize=18)
            fig.suptitle("%s %s %s \n DOP = %s Obs. > %s" 
                    %(station, mn, year, dop_thresh, minObs), size=22)
    plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
