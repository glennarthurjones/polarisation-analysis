#!/usr/bin/env python 
# Python script to read the yearly summary files plot the results  
import os
import argparse
import datetime
import numpy as np
import matplotlib.pyplot as plt
from calendar import monthrange, datetime
from pandas import read_csv
from matplotlib import colors

#---------------------------------------------------------------------
def getNumDays(year):
    soy = datetime.date(year,1,1)
    eoy = datetime.date(year+1,1,1)
    days_in_year = (eoy - soy).days
    return days_in_year

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def makeHist3d(dta, doyBins, freqBins, bazBins):
    '''
    Generate the bin data for the baz histogram
    '''
    ax = len(doyBins)-1
    ay = len(freqBins)-1
    doyFreqBaz = np.zeros([ax,ay])
    for ii in range(0,ax):
        for jj in range(0,ay):
            aziHist = dta[ii, jj, :]
            idx = np.where(abs(aziHist) == abs(aziHist).max())[0][0]
            doyFreqBaz[ii,jj] = bazBins[idx]

    return doyFreqBaz
#---------------------------------------------------------------------
def main():
    '''
    Script to plot yearly summaries of the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                "Year summary plots of the results of H/V analysis.\n"
                "Must have performed monthly and yearly summary prior \n"
                "to running command",
                formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--years', nargs='+',
                    help='years to process', required=True )
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)

    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    years = args['years']
    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']
    day_sec = 24.*60.*60.
  
    # variable names
    vN = ["azi", "freq", "dop", "doy", "time", "hv"]
    # loop over the years
    for kk, year in enumerate(years):
        for loc in locID:
            fnameIn = ('%s/%s_%s_%s_summary.txt' 
                    %(station, year,measurement_code, loc))
            
            if not os.path.isfile(fnameIn):
                print 'ERROR: file %s does not exist' %fnameIn
                return
           
            # use pandas to open the folder
            pF  = read_csv(fnameIn, sep=",", comment="#", names = vN)
            azi = pF.azi.values
            freq = pF.freq.values
            dop = pF.dop.values
            doy = pF.doy.values
            time = pF.time.values
            hv = pF.hv.values
            # pandas file cleanup
            del pF
            # compute the frequency list
            freq_list = list(np.unique(freq))
            doyF = doy[dop >= dop_thresh]
            freqF = freq[dop >= dop_thresh]
            baz = np.array(azi[dop >= dop_thresh])
            baz = flipBaz(baz) # convert between 0 and 360

            # compute the period 
            T = []
            for f in freqF:
	        t=1./f #compute the period
                T.append(t)

            # DOP based filtering on the variables of interest
            doy_max = getNumDays(int(year))
            # plot of frequency vs day of year  
            fig = plt.figure(2*kk, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            axP = plt.subplot(311)
            
            fBins = np.linspace(np.min(freq_list), np.max(freq_list), 25)
            dBins = np.arange(1, doy_max+1,2) # averaging over 2 days
            freqBin, dayBin = np.meshgrid(fBins,dBins)
            # compute the 2D histograms
            [doyFreqH, xbin, ybin] = np.histogram2d(x=doyF, y=freqF, bins=[dBins,fBins])
            # normalise each day by the number of observations
            doyFreqSum = doyFreqH.sum(axis=1) 
            doyFreqN = doyFreqH / doyFreqSum[:,None]  
            # plot the results
            normC = colors.Normalize(vmin=0, vmax=0.5)
    
            pc=axP.pcolor(dayBin, freqBin, doyFreqN, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r', norm=normC)           
            
            xTicks = np.arange(0,361,60)
            xTicks[0]=1
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks,xTickMark)
            plt.xlim(1,doy_max) 
            plt.xlabel('Day of Year', size=16)

            plt.ylim(min(freq_list), max(freq_list))
            plt.ylabel('Frequency (Hz)', size=16)
            cbar = plt.colorbar(pc)
            cbar.set_label('Normalised count', fontsize=16)
            cticks = np.arange(0,0.51,0.1)
            ctickMark = ["%s" % x for x in cticks]
            cbar.set_ticks(cticks)
            cbar.set_ticklabels(ctickMark)

            #=====================================================
            # plot of frequency vs day of year coloured by azimuth 
            axP1 = plt.subplot(312)
            # arrange the data so we can make it into a histogram
            deltaA = 5
            hBins = np.arange(0,361, deltaA)
            [doyFreqA,edges] = np.histogramdd([doyF, freqF, baz], 
                    bins=[dBins, fBins, hBins] )
                
            doyFreqAzi = makeHist3d(doyFreqA, dBins, fBins, hBins)
            cmap = plt.get_cmap('hot_r',len(hBins))
            pc=axP1.pcolor(dayBin, freqBin, doyFreqAzi, 
                    edgecolors='w', linewidths=0.25,
                    cmap=cmap)           
            
            xTicks = np.arange(0,361,60)
            xTicks[0]=1
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks,xTickMark)
            plt.xlim(1,doy_max) 
            plt.xlabel('Day of Year', size=16)

            plt.ylim(min(freq_list), max(freq_list))
            plt.ylabel('Frequency (Hz)', size=16)
            cbar = plt.colorbar(pc)
            cbar.set_label('Back azimuth ($^\circ$)', fontsize=16)
            cbar.set_clim([0, 360])
            cbar.set_ticks([0, 180, 355])
            cbar.set_ticklabels(['0','180', '360'])
            #=====================================================
            # plot the back azimuth vs day of year
            axP2 = plt.subplot(313)
            # collapse the 3d histogram so we have it as baz v doy
            doyAzi = doyFreqA.sum(axis=1)
            doyAziSum = doyAzi.sum(axis=1) 
            doyAziN = doyAzi / doyAziSum[:,None]  
            # plot the results
            normC = colors.Normalize(vmin=0, vmax=0.2)
            aziBin, dayBin = np.meshgrid(hBins,dBins)
            pc=axP2.pcolor(dayBin, aziBin, doyAziN, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r', norm=normC)    
            
            xTicks = np.arange(0,361,60)
            xTicks[0]=1
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks,xTickMark)
            plt.xlim(1,doy_max) 
            plt.xlabel('Day of Year', size=16)

            plt.ylim([0, 360])
            yTicks = [0, 90, 180, 270, 360]
            yTickMark = ["%s" % y for y in yTicks]
            plt.yticks(yTicks,yTickMark)
            plt.ylabel('Back azimuth ($^\circ$)', size=16)
            cbar = plt.colorbar(pc)
            cbar.set_label('Normalised count', fontsize=16)
            cticks = np.arange(0,0.21,0.05)
            ctickMark = ["%s" % x for x in cticks]
            cbar.set_ticks(cticks)
            cbar.set_ticklabels(ctickMark)
            
            fig.suptitle(" %s %s \n DOP = %s" %( station, year, dop_thresh),
                    size=22)
            plt.subplots_adjust(hspace=0.3)

            #=====================================================
            # New figure of the frequency and back azimuth vs DOY
            # This plot will not include normalised amplitudes
            fig = plt.figure(2*kk+1, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            axP = plt.subplot(211)
            
            fBins = np.linspace(np.min(freq_list), np.max(freq_list), 25)
            dBins = np.arange(1, doy_max+1,2) # averaging over 2 days
            freqBin, dayBin = np.meshgrid(fBins,dBins)
            # compute the 2D histograms
            [doyFreqH, xbin, ybin] = np.histogram2d(x=doyF, y=freqF, bins=[dBins,fBins])
            # plot the results
            pc=axP.pcolor(dayBin, freqBin, doyFreqH, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r', vmin=0)           
            
            xTicks = np.arange(0,361,60)
            xTicks[0]=1
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks,xTickMark)
            plt.xlim(1,doy_max) 
            plt.xlabel('Day of Year', size=16)

            plt.ylim(min(freq_list), max(freq_list))
            plt.ylabel('Frequency (Hz)', size=16)
            cbar = plt.colorbar(pc)
            cbar.set_label('Count', fontsize=16)
            # Find out what base to the power of 10 the max histogram 
            # values is
            histBase10 = 10**(np.floor(np.log10(doyFreqH.max())))
            ctickMax = np.around(doyFreqH.max()/histBase10,1)*histBase10
            cticks = np.linspace(0,ctickMax,6)
            ctickMark = ["%s" % x for x in cticks]
            cbar.set_ticks(cticks)
            cbar.set_ticklabels(ctickMark)
            cbar.set_clim([0, doyFreqH.max()]) 
            axP2 = plt.subplot(212)
            # collapse the 3d histogram so we have it as baz v doy
            doyAzi = doyFreqA.sum(axis=1)
            
            
            # plot the results
            aziBin, dayBin = np.meshgrid(hBins,dBins)
            pc=axP2.pcolor(dayBin, aziBin, doyAzi, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r', vmin=0)    
            
            xTicks = np.arange(0,361,60)
            xTicks[0]=1
            xTickMark = ["%s" % x for x in xTicks]
            plt.xticks(xTicks,xTickMark)
            plt.xlim(1,doy_max) 
            plt.xlabel('Day of Year', size=16)

            plt.ylim([0, 360])
            yTicks = [0, 90, 180, 270, 360]
            yTickMark = ["%s" % y for y in yTicks]
            plt.yticks(yTicks,yTickMark)
            plt.ylabel('Back azimuth ($^\circ$)', size=16)
            cbar = plt.colorbar(pc)
            cbar.set_label('Count', fontsize=16)
            histBase10 = 10**(np.floor(np.log10(doyAzi.max())))
            ctickMax = np.around(doyAzi.max()/histBase10,1)*histBase10
            cticks = np.linspace(0,ctickMax,6)
            ctickMark = ["%s" % x for x in cticks]
            cbar.set_ticks(cticks)
            cbar.set_ticklabels(ctickMark)

            fig.suptitle(" %s %s \n DOP = %s" %( station, year, dop_thresh),
                    size=22)
            plt.subplots_adjust(hspace=0.3)

            plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
