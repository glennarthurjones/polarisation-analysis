#!/usr/bin/env python 
# python script to plot the h/v results on a month by month basis
import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
from mpl_toolkits.basemap import Basemap, addcyclic, cm
from obspy import read_inventory, read
from obspy.geodetics.base import gps2dist_azimuth
from obspy.imaging.beachball import beach
from matplotlib import colors
from scipy.ndimage.filters import minimum_filter, maximum_filter
from netCDF4 import Dataset as NetCDFFile
from calendar import month_name
from mpl_toolkits.axes_grid.inset_locator import InsetPosition
from mpl_toolkits.axes_grid1 import make_axes_locatable
#---------------------------------------------------------------------
# get the min-max values for the sea-pressure levels
def extrema(mat,mode='wrap',window=10):
    """find the indices of local extrema (min and max)
        in the input array."""
    mn = minimum_filter(mat, size=window, mode=mode)
    mx = maximum_filter(mat, size=window, mode=mode)
    # (mat == mx) true if pixel is equal to the local max
    # (mat == mn) true if pixel is equal to the local in
    # Return the indices of the maxima, minima
    return np.nonzero(mat == mn), np.nonzero(mat == mx)

#---------------------------------------------------------------------
# convert between cartesian and polar coordinates
def cart2pol(x, y):
    ''' convert between cartesian to polar coordinates'''
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    ''' convert between polar to catresian coordinates'''
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

#---------------------------------------------------------------------
# compute the lat and lon given a bearing and distance 
def getLatLon(lat0, lon0, theta, d):
    ''' compute the lat and lon from a point given a bearing and azimuth

        Reference: http://www.movable-type.co.uk/scripts/latlong.html
    '''
    R = 6378.1 #Radius of the Earth
    # convert lat0 and lon0 to radian
    phi1 = np.deg2rad(lat0)
    lambda1 = np.deg2rad(lon0)
    theta = np.deg2rad(theta)
    delta = d/R

    # compute the latitude
    phi2 = np.arcsin( np.sin(phi1) * np.cos(delta) + 
            np.cos(phi1) * np.sin(delta) * np.cos(theta))
    
    a = np.sin(theta) * np.sin(delta) * np.cos(phi1)
    b = np.cos(delta) - np.sin(phi1) * np.sin(phi2)

    lambda2 = lambda1 + np.arctan2(a, b)

    lat2 = np.rad2deg(phi2)
    lon2 = np.rad2deg(lambda2)

    return lat2, lon2

#---------------------------------------------------------------------
def main():
    ''' 
    Main script to plot monthly results from the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                                    'Plot the monthly H/V results')
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
    parser.add_argument('--channel', type=str,
                    help='Channel name to get the station info.\n'
                    'Default is BHZ', required=False)
       
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)

    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']

    if args['channel'] is None:
        channel='BHZ'
    else:
        channel=args['channel']

    day_sec = 24.*60.*60.
    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = np.loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            freq_list = list(np.unique(freq))
            # compute the period 
            T = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
 

            # filter the results based on the DOP threshold
            # convert T, time and hv into numpy arrays
            T = np.array(T)
            baz = np.array(azi[dop >= dop_thresh]) 
            freqD = freq[dop >= dop_thresh]

            # make a polar histogram plot 
            fig = plt.figure(kk, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=14
            plt.rcParams['ytick.labelsize']=14
            # histogram of the back azimuth
            nBins = 360/10
            hBins = np.arange(-180,190,10) # create bins centred on the values
            axP = plt.subplot(121, polar=True)
            # setting the axis to be in geographical coordinates
            axP.set_theta_direction(-1)
            axP.set_theta_offset(np.pi/2.0)
            # generate a polar histogram of the back azimuth as a function of frequency
            deltaF = 0.01    
            fBins = np.linspace(np.min(freq_list), np.max(freq_list), 25)
            deltaA = np.deg2rad(5)
            aBins = np.arange(-np.pi, np.pi+deltaA, deltaA) # angular bins
            thetaBin, rBin = np.meshgrid(aBins, fBins)
            bazR = np.deg2rad(baz)

            # compute the 2D histogram 
            [bazFreqH, xbin, ybin] = np.histogram2d(x=freqD, y=bazR, 
                    bins=[fBins, aBins])
            # normalize the plot based on the maximum  
            bazFreqMax = bazFreqH.max()
            bazFreqMin = bazFreqH.min()
            bazFreqN = (bazFreqH -bazFreqMin) / (bazFreqMax - bazFreqMin)  
            # plot the results
            normC = colors.Normalize(vmin=bazFreqMin, vmax=bazFreqMax)
    
            pc=axP.pcolor(thetaBin, rBin, bazFreqN, 
                    edgecolors='w', linewidths=0.25,
                    cmap='Spectral_r')
            # mask values below the minimum we have 
            axP.set_ylim([0, 0.5])
            #set the angular tick labels
            thetaticks = np.arange(0,360,45)
            axP.set_thetagrids(thetaticks, frac=1.15) 
            #cbar = plt.colorbar(pc,pad=0.1)
            cbar = plt.colorbar(pc,pad=0.11, fraction=0.046)
            cbar.set_label('Normalised count', fontsize=16)
            # remove the radial tick labels
            axP.set_yticklabels([])

            # get the station information
            staName = (glob.glob('%s/*/resp/STXML*.%s.%s' 
                    %(dirname , loc, channel)))
            if staName:
                staName = staName[0]
            else:
                error = ("No information about %s for %s"
                    %(station, month_name[int(month)]))
                print('ERROR: %s' %error)
                continue

            staDta = read_inventory(staName)
            lats=staDta[0][0].latitude
            lons=staDta[0][0].longitude
            # sea pressure information
            spFile = 'SeaPressure/%s/sp-%02d.nc' %(year, int(month))
            nc = NetCDFFile(spFile)
            lats1 = nc.variables['lat'][:]
            lons1 = nc.variables['lon'][:]
            nlats = len(lats1)
            nlons = len(lons1)
            # read prmsl, convert to hPa (mb).
            slp = nc.variables['slp'][0]
            # the window parameter controls the number of highs and lows detected.
            # (higher value, fewer highs and lows)
            local_min, local_max = extrema(slp, mode='wrap', window=50)

            ax1 = plt.subplot(122, polar=True)
            # first we plot a histogram of the normalised amplitudes 
            btm = 9.
            maxV = 1.

            [n_all, bins_all] = np.histogram(bazR, aBins)
            n_min = float(n_all.min())
            n_max = float(n_all.max())
            n_norm = (maxV - 0.) * ((n_all - n_min)/(n_max - n_min)) 
#            n_norm = n_all/float(n_all.max()) # normalize the histogram 
#            idx = np.where(n_norm >= 0.5)[0] #get the bins which are 50% of the max
            mids = bins_all[:-1] + np.diff(bins_all)/2.
            bars = ax1.bar(mids, n_norm, width=deltaA, bottom=btm, 
                     alpha=0.5, color='blue')
            ax1.set_theta_direction(-1)
            ax1.set_theta_offset(np.pi/2.0)
          
            # tidy up the axes
            ax1.grid(False)
            ax1.set_rticks([])
            ax1.set_xticks([])

            # insert axis which will have the air pressure map
            ax2 = plt.axes([0,0,1,1])
#            ip = InsetPosition(axP, [0.1,0.1,0.8,0.8])
            ip = InsetPosition(ax1, [0.05,0.05,0.9,0.9])
            ax2.set_axes_locator(ip)

#            m= Basemap(projection='stere' , width=17000000, height=17000000, 
#                        lat_0 = lats, lon_0 = lons, round=True) # plot the station location
            m = Basemap(projection='npaeqd', boundinglat=20, 
                    lon_0=lons, lat_0=lats, round=True)
            latsSta=staDta[0][0].latitude
            lonsSta=staDta[0][0].longitude
            xS, yS =  m(lonsSta, latsSta)
            m.scatter(xS,yS,200,color="r",marker="v",edgecolor="k",zorder=3)
    
#            # add the sea-pressure information
            slp, lons1 = addcyclic(slp, lons1)
            # find x, y of map projection grid
            lons1, lats1 = np.meshgrid(lons1,lats1)
            x, y = m(lons1,lats1)
            # set the color scheme to be between the min and max values of the 
            # sea-pressure infomation
            cmapR = np.round(np.linspace(np.mean(slp)-np.std(slp), 
                    np.mean(slp) + np.std(slp), 10)) 
            cs = m.contourf(x,y,slp,
                    cmapR, cmap='viridis',extend='both')
            cs.cmap.set_under(cs.cmap.colors[0])
            cs.cmap.set_over(cs.cmap.colors[-1])
#            cs = m.contourf(x,y,slp,
#                    np.arange(np.floor(np.min(slp)),np.floor(np.max(slp)),5),
#                    cmap='viridis')
#            m.shadedrelief()
            m.drawcoastlines()
             
            # histogram of the azimuth computations and find the angle where this is a max
#            [n_all, bins_all] = np.histogram(bazR, aBins)
#            n_norm = n_all/float(n_all.max()) # normalize the histogram 
#            idx = np.where(n_norm >= 0.5)[0] #get the bins which are 50% of the max
#            mids = bins_all[:-1] + np.diff(bins_all)/2.
#            bins_n = mids[idx]
#            
#            latP, lonP = getLatLon(latsSta, lonsSta, np.rad2deg(bins_n), 5000)
#            # compute new lat and lon from station along the azimuth
#            xP, yP = m(lonP, latP)    
#            
#            # scatter plot with the colors giving the normalised histogram count
#            cmap = plt.get_cmap('hot',10)
#            bounds = np.linspace(0.5,1.0,11)
#            norm = colors.BoundaryNorm(bounds, cmap.N) 
#            
#            css = m.scatter(xP, yP, 40,marker='s', c=n_norm[idx], cmap=cmap, norm=norm,
#                    edgecolor="face")
#            
#            # colorbar
#            cb2 = plt.colorbar(css, ticks=np.linspace(0.5,1,6), 
#                    pad=0.2, fraction=0.046)
#            cb2.set_label("Normalised count",size=16)   
#            cb = plt.colorbar(cs,fraction=0.06)

            # get the bounds of the colrbar generated for the polar histogram
            # axP
            lc, bc, wc, hc = cbar.ax.get_position().bounds
            # now get the location of the axis used to plot a summary of
            # the polar histogram ax1
            l1, b1, w1, h1 = ax1.get_position().bounds

            # now we must generate a new axis for the colorbar to go into
#            cax = fig.add_axes([0.91, bc, wc, hc])
            cax = fig.add_axes([0.91, 0.241, wc, 0.66*h1])
#
            cb = plt.colorbar(cs, cax=cax)
            cb.set_label("Air Pressure at Sea Level (mb)",size=16)

            mn=month_name[int(month)]
            fig.suptitle("%s %s \n DOP = %s" %(mn, year, dop_thresh), size=22)
            plt.subplots_adjust(left=0.05, wspace=0.18)
    plt.show()    

#---------------------------------------------------------------------
if __name__=="__main__":
    main()
