#!/usr/bin/env python
#
# Python script to generate an initial velocity model for the H/V
# inversion based on litho1.0 model
#
import os
import glob
import sys
import argparse
import numpy as np
import matplotlib.pylab as plt
from collections import defaultdict
from calendar import monthrange
from obspy.core.inventory import read_inventory
#======================================
def readLitho(fname):
    '''
    Read lith 1.0 output files
    '''
    depthT = []
    vp = []
    vs = []
    rho = []
    f = open(fname, 'r')
    lines = f.readlines()
    f.close()
    for l in lines:
        dta = l.split()
        depthT.append(dta[0])
        rho.append(dta[1])
        vp.append(dta[2]) 
        vs.append(dta[3])

    # fine the minimum depth which is defined +ve down and use this as the
    # datum
    datum = min([float(x) for x in depthT])
    # iterate over the list and add the datum 
    depth = [float(x) - datum for x in depthT] 
    return vp, vs, rho, depth, datum

#======================================
def outputVmodel(model,fname):
    '''
    Build the output model to be compatible with the ellipticity 
    modelling code 
    '''
    H = np.hstack([model[0,0], np.diff(model[:,0])])
    
    # open the file and populate it with the header information
    f = open(fname,'w+')
    f.write('MODEL.01\n')
    f.write('MODELLO DEMO\n')
    f.write('ISOTROPIC\n')
    f.write('KGS\n')
    f.write('SPHERICAL EARTH\n')
    f.write('1-D\n')
    f.write('CONSTANT VELOCITY\n')
    f.write('LINE08\n')
    f.write('LINE09\n')
    f.write('LINE10\n')
    f.write('LINE11\n')
    varHead = (('%-7s %-7s %-7s %-7s %-7s %-7s %-7s %-7s %-7s %-7s\n')
            %('H', 'VP', 'VS', 'RHO', 'QP', 'QS', 
                'ETAP', 'ETAS', 'FREFP', 'FREFS'))
    f.write(varHead)
    
    # loop over the model we wish to save
    for ii in range(0,len(model)):
        if ii == len(model)-1:
            f.write(('%-7.2f %-7.4f %-7.4f %-7.4f '  
                '%-7.1f %-7.1f %-7.1f %-7.1f %-7.1f %-7.1f')
                % (H[ii], model[ii,1], model[ii,2], model[ii,3],
                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0))
        else:
            f.write(('%-7.2f %-7.4f %-7.4f %-7.4f '  
                '%-7.1f %-7.1f %-7.1f %-7.1f %-7.1f %-7.1f\n')
                % (H[ii], model[ii,1], model[ii,2], model[ii,3],
                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0))
    f.close()

#======================================
def plotVmodel(vpL, vsL, depthL, station):
    '''
    Function to plot the velocity model
    '''

    fig = plt.figure(figsize=(11.69,8.27)) 
    plt.rcParams['xtick.labelsize']=14
    plt.rcParams['ytick.labelsize']=14
    plt.subplot(121)
    plt.plot(np.array(vpL,dtype=float)/1000., -np.array(depthL,dtype=float)/1000., 
            '-', color='blue', marker='o',
            linewidth=3, label="Litho1.0")
    
    plt.ylim([-45, 0])
    plt.xlim([3, 8])
    yTicks = np.arange(0,-50,-5)
    yTickMark = ["%s" % -x for x in yTicks]
    yTicks[0]=-1
    plt.yticks(yTicks,yTickMark)
    plt.xlabel("Vp (km/s)", fontsize=15)
    plt.ylabel("Depth (km)", fontsize=15)
    plt.legend(loc=3, fontsize=16)
    plt.grid()
    
    
    plt.subplot(122)
    plt.plot(np.array(vsL,dtype=float)/1000., -np.array(depthL,dtype=float)/1000., 
            '-', color='blue', marker='o',
            linewidth=3, label="Litho1.0")
    
    plt.ylim([-45, 0])
    plt.xlim([1, 5])
    yTicks = np.arange(0,-50,-5)
    yTickMark = ["%s" % -x for x in yTicks]
    yTicks[0]=-1
    plt.yticks(yTicks,yTickMark)
    plt.xlabel("Vs (km/s)", fontsize=15)
    plt.ylabel("Depth (km)", fontsize=15)
    plt.legend(loc=3, fontsize=16)
    plt.grid()
    fig.suptitle('Litho1.0 at '+station, fontsize=20) 
    
    plt.show()

#---------------------------------------------------------------------
def main():
    '''
    Script to get the litho1.0 velocity model for a station and plot
    '''

    parser = argparse.ArgumentParser(description=
            "Script to get the litho1.0 velocity model for a station and plot",
            formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--stations', type=str, nargs='+',
                    help='stations name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--month', nargs=1,
                    help='month to process', required=True)
    parser.add_argument('--channel', type=str,
                    help='Channel name to get the station info.\n'
                    'Default is BHZ', required=False)

     # setting up the variables to plot
    args = vars(parser.parse_args())
    stations = args['stations']
    year = args['year'][0]
    month = args['month'][0]
    if len(month) > 1:
        month.sort(key=int)
    if args['channel'] is None:
        channel='BHZ'
    else:
        channel=args['channel']

    # loop over the stations
    for station in stations:
        base_dir = '%s/%s.%02d' %(station, year, int(month))
        # get the number of days in the year
        nDays = monthrange(int(year), int(month))[1]
        days = ['%02d' % (jj+1) for jj in range(0,nDays)]
#        # loop over the days and find the 1st present
#        for day in days:
#            dirname = "%s/%s" % (base_dir, day)
#            if not os.path.isdir(dirname):
#                continue
#            # loop over the locs file and get the station information
#            staName = glob.glob('%s/resp/STXML*.%s' % (dirname, channel))[0] 
#            staDta = read_inventory(staName)
#            lat=staDta[0][0].latitude
#            lon=staDta[0][0].longitude
#            print '--> Getting litho model for %s located at lat=%s lon=%s' \
#                    %(station, lat, lon)
#            litho1 = "%s.litho1.txt" % station
#            # check to see if the output inversion directory exists
#            if not os.path.isdir("%s/Inversion" % station): 
#                os.makedirs("%s/Inversion" % station)
#
#            # compute the results using litho1.0
#            try:
#                lithoStr = ("access_litho -p %s %s > %s/Inversion/%s" 
#                     %(lat, lon, station, litho1))
#                os.system(lithoStr)
#            except Exception as error:
#                print('WARNING: %s' %error)
#                continue
#            break 

        # get the station information
        staName = (glob.glob('%s/*/resp/STXML*.%s' 
                %(base_dir , channel)))
        if staName:
            staName = staName[0]
        else:
            error = ("No information about %s for %s"
                %(station, month_name[int(month)]))
            print('ERROR: %s' %error)
            sys.exit() 

        staDta = read_inventory(staName)
        lat=staDta[0][0].latitude
        lon=staDta[0][0].longitude
        print '--> Getting litho model for %s located at lat=%s lon=%s' \
                %(station, lat, lon)
        litho1 = "%s.litho1.txt" % station
        # check to see if the output inversion directory exists
        if not os.path.isdir("%s/Inversion" % station): 
            os.makedirs("%s/Inversion" % station)

        # compute the results using litho1.0
        try:
            lithoStr = ("access_litho -p %s %s > %s/Inversion/%s" 
                    %(lat, lon, station, litho1))
            os.system(lithoStr)
        except Exception as error:
            print('WARNING: %s' %error)
            sys.exit() 

        # read the velocity model
        vmodelIn = '%s/Inversion/%s' %(station,litho1)
        vpL, vsL, rhoL, depthL, datumL = readLitho(vmodelIn)
        # Compute the layer thickness and associated values
        depthLayer = depthL[::-1][1::2]
        vpLayer = vpL[::-1][1::2]
        vsLayer =  vsL[::-1][1::2]
        rhoLayer =  rhoL[::-1][1::2]

        # sort the depths and the other variables
        lithoArr = np.column_stack(
                    [depthLayer, vpLayer, vsLayer, rhoLayer]).astype(float)

        # we are only interested in the bottom layer values 
        lithoArrS = lithoArr/1000.
#        lithoArr = lithoArr[1::2,:]/1000.
#        lithoArrS = lithoArr[lithoArr[:,0].argsort()]

        # save the models
        outLitho = '%s/Inversion/%s.litho1.0.d' %(station, station)
        outputVmodel(lithoArrS,outLitho)

        plotVmodel(vpL, vsL, depthL, station)

#---------------------------------------------------------------------
if __name__=="__main__":
    main()


