#!/usr/bin/env python 
# Python script to plot the results of the H/V analysis for 
# an Earthquake. The goal is to see if the low frequencie
# azimuths correspond with the earthquake
import os 
import sys 
import glob
import argparse
import numpy as np
import matplotlib.pyplot as plt
import datetime
from mpl_toolkits.basemap import Basemap
from obspy import read_inventory, read, UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth
from obspy.imaging.beachball import beach
from matplotlib import colors
#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
# load the trace data and trim appropriatley 
def getTraces(fname, locID, t0, tE):
    '''
    Use obspy to read the traces and do some processing 
    '''
    dtaFolder = "%s/processed/*.%s*BHZ*.sac" %(fname, locID)
    st = read(dtaFolder)
    st += read(dtaFolder.replace("BHZ", "BHN"))
    st += read(dtaFolder.replace("BHZ", "BHE"))
    st = st.merge()
    st = st.sort()
    # trim the traces 
    st.trim(t0, tE, nearest_sample=False)

    return st
#---------------------------------------------------------------------
def main():
    '''
    Script to generate a summary plot from the Event based polarisation 
    processing
    '''
    parser = argparse.ArgumentParser(description=
                        "Summary of event based data processing",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.8', required=False)
   
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    if len(months) > 1:
        months.sort(key=int)
    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.8
    else:
        dop_thresh = args['dop']


#    time_h = 2
#    time_sec = time_h*60*60.   
    c = 0
    for jj, month in enumerate(months):
        dirname = "%s/%s.%02d/Event/" %(station, year, int(month))
        eventList = os.listdir(dirname)
        events = [os.path.basename(x) for x in glob.glob(dirname + '/%s%02d*' 
                    %(year, int(month)))]
        for kk, event in enumerate(events):
            event_folder = dirname + "%s" % (event)   
            for loc in locID: 
                fname= ("%s/%s.%02d.%s.%s*" 
                    %(event_folder,year, int(month), measurement_code, loc))
                filename = glob.glob(fname)

                if not filename:
                    continue
                for ii, fl in enumerate(filename):
                    flB = os.path.basename(fl)
                    loc = flB.split('.')[3]
                    # output variables 
                    azi, freq, freq_list, dop, time, hv =[],[],[],[],[],[]
                    with open(fl) as fp:
                        for line in fp:
                            if line.split()[0]== year:
	                        pass
	                    else:
	                        azi_tmp = float(line.split()[0])
	                        freq_tmp = float(line.split()[1])
	                        dop_tmp = float(line.split()[2])
	                        time_tmp = float(line.split()[3])
	                        hv_tmp = np.log10(float(line.split()[5]))
	            	    		
	                        # check to make sure the results are within our DOP
	                        if dop_tmp >= dop_thresh:
	                            azi.append(azi_tmp)
	            	            freq.append(freq_tmp)
	            	            dop.append(dop_tmp)
	            	            time.append(time_tmp)
	            	            hv.append(hv_tmp)

	            	            # check to see if there are unique frequency values
	            	            if freq_tmp not in freq_list:
	            	                freq_list.append(freq_tmp)

                    # compute the period  
                    T = []
                    for f in freq:
	                t=1./f #compute the period
                        T.append(t)
  
                    # convert T, time and hv into numpy arrays
                    T = np.array(T)
                    freq = np.array(freq)
                    time = np.array(time) 
                    hv = np.array(hv)
                    baz = np.array(azi)
                    dop = np.array(dop)
                
                    # compute the EQ time in seconds
                    eventInfo = np.load(event_folder + '/../EVENTS-INFO/event_list_pickle')
                    for eI in eventInfo:
                        if eI['event_id']==event_folder.split('/')[-1]:
                            day = eI['datetime'].day
                            [hh,mm,ss] = eI['datetime'].time.isoformat().split(":")
                            EQtimeSec = datetime.timedelta(hours=int(hh), 
                                    minutes=int(mm), seconds=int(float(ss))).total_seconds()
                            # get the lat and lon f the EQ
                            EQlons = eI['longitude']
                            EQlats = eI['latitude']
                            EQdepth = eI['depth']
                            EQmag = eI['magnitude']
                            EQmagT = eI['magnitude_type']
                            EQt = eI['datetime'] 
                            EQtimes = "%02d:%02d:%02d" %(EQt.hour, EQt.minute, EQt.second) 
                            MT = eI['focal_mechanism']

#---------------------------------------------------------------------
# Plot the results
                fig=plt.figure(c, figsize=(12, 9) )
                plt.rcParams['xtick.labelsize']=14
                plt.rcParams['ytick.labelsize']=14
       
                plt.subplot2grid((6,2),(0,0), rowspan=3)  
                #locID = flB.split('.')[3]
                staName = glob.glob("%s/resp/STXML*%s.BHZ" %(event_folder, loc))[0]
                staDta = read_inventory(staName)
                lats=staDta[0][0].latitude
                lons=staDta[0][0].longitude

                m= Basemap(projection='aeqd', lat_0 = lats, lon_0 = lons, round=True)
                m.shadedrelief(scale=0.1)
    
                x, y =  m(lons, lats)
                m.scatter(x,y,200,color="r",marker="v",edgecolor="k",zorder=3)
    
                x, y = m(EQlons, EQlats) 
                # plot the great circles
                m.drawgreatcircle(lons,lats,EQlons,EQlats,
                    linewidth=2,color='k',alpha=0.4)
                ax =plt.gca()
                b = beach(MT, xy=(x,y), width=2000000, linewidth=1.5)
                #b.set_zorder(10)
                ax.add_collection(b)
                #=========================================
                # Polar histogram plot
                axP = plt.subplot2grid((6,2), (0, 1), rowspan=3, polar=True)
                # setting the axis to be in geographical coordinates
                axP.set_theta_direction(-1)
                axP.set_theta_offset(np.pi/2.0)
                # generate a polar histogram of the back azimuth as a function of frequency
                deltaF = 0.01    
                #fBins = np.linspace(min(freq_list), max(freq_list), 20)
                fBins = np.linspace(np.min(freq_list), np.max(freq_list),25)
                deltaA = np.deg2rad(5)
                aBins = np.arange(-np.pi, np.pi+deltaA, deltaA) # angular bins
                thetaBin, rBin = np.meshgrid(aBins, fBins)
                bazR = np.deg2rad(baz)

                # compute the 2D histogram 
                [bazFreqF, xbin, ybin] = np.histogram2d(x=freq, y=bazR, 
                        bins=[fBins, aBins])
                # normalize the plot based on the maximum  
                bazFreqMax = bazFreqF.max()
                bazFreqMin = bazFreqF.min()
                bazFreqN = (bazFreqF -bazFreqMin) / (bazFreqMax - bazFreqMin)  
                # plot the results
#                normC = colors.Normalize(vmin=bazFreqMin, vmax=bazFreqMax)
    
                normC = colors.LogNorm(vmin=1.0, vmax=bazFreqMax)
                pc=axP.pcolor(thetaBin, rBin, bazFreqF, 
                        edgecolors='w', linewidths=0.25,
                        cmap='Spectral_r')
                # mask values below the minimum we have 
                axP.set_ylim([0, 0.3])
                #set the angular tick labels
                thetaticks = np.arange(0,360,45)
                axP.set_thetagrids(thetaticks, frac=1.15) 
                cbar = plt.colorbar(pc,pad=0.11, fraction=0.046)
                cbar.set_label('Count', fontsize=16)
                # remove the radial tick labels
                axP.set_yticklabels([])
                
                # get the great circle information
                [gc_dist, gc_baz, gc_az] = gps2dist_azimuth(lats, lons, 
                        EQlats, EQlons)

                plt.plot(np.deg2rad([gc_baz, gc_baz]), [min(freq_list), max(freq_list)],
                        '-k', color='#D3D3D3', linewidth=1.5)

                #=========================================
                bazF = flipBaz(baz)
                # scatter plot of freq v time coloured by BAZ 
                plt.subplot2grid((6,2), (3,0), rowspan=3)
                plt.scatter(freq, time, 20,c = baz, marker=".",cmap="jet",
                        edgecolor="face", vmin=0, vmax=360)    
                cb = plt.colorbar()
                cb.ax.set_title("BAZ ($^\circ$)",size=14)
                # get the hour tick marks 
                time_h = np.ceil(np.max(time)/(60*60.))        
                time_sec = time_h*60*60.
                hourTick = np.arange(0,time_h + 0.5,0.5).tolist()
                hourTickMark = ["%s" % x for x in hourTick]
                hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
                plt.yticks(hourTick,hourTickMark)
                #plt.xlim([0.015, 0.2])
                #plt.xlim([min(freq_list), max(freq_list)])
                plt.xlim([min(freq_list),0.2])
                plt.ylim([0, time_sec])
                plt.ylabel("Time (Hours)",size=16)
                plt.xlabel("Frequency (Hz)", size=16)
                plt.grid() 

                #=========================================
                # Plot the traces used to analyise the data
                dtaFolder = '%s/processed/*.%s*BHZ*.sac' %(event_folder, loc)
                st = read(dtaFolder)        
                st += read(dtaFolder.replace("BHZ", "BHN"))
                st += read(dtaFolder.replace("BHZ", "BHE"))
                st = st.merge()
                st = st.sort()
                for q, tr in enumerate(st):
                    comp = tr.stats.channel
                    plt.subplot2grid((6,2),(q+3,1))
                    
                    trTime = np.arange(0, tr.stats.npts, 1)*tr.stats.delta
                    trTime_h = trTime/60.
                    hourTick = np.arange(0,time_h + 1,0.5).tolist()
           
                    plt.plot(trTime, tr.data, '-k', linewidth=2)

                    # set the y axis to have only 3 ticks 
                    yT = np.array([min(plt.yticks()[0]), 0,max(plt.yticks()[0])])
                    yTMark = ["%1.1e" %x for x in yT] 
                    plt.yticks(yT, yTMark)
                    if q == 1:
                        plt.ylabel('Displacement (m)', size=16)
                    
                    if q == 2:
                        # setting up the tick labels and markers
                        hourTickMark = ["%s" % x for x in hourTick]
                        hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
                        plt.xticks(hourTick,hourTickMark)
                        plt.xlabel('Time (Hours)', size=16)
                    else: 
                        hourTickMark = ["" for x in hourTick]
                        hourTick = np.multiply(hourTick,60.*60.).tolist() # convert to seconds 
                        plt.xticks(hourTick,hourTickMark)
          
                    plt.xlim([0, 2*60.*60.])
                    plt.legend(['%s' %comp], loc='upper right', framealpha=0.7)
                    plt.grid()

                plt.subplots_adjust(left=0.1, bottom=0.07, right=0.92, 
                    wspace=0.30, hspace=0.4)
                fig.suptitle("%s:%s %02d-%s-%s" %(station,loc, day,month, year), size=18)

                plt.show()
                c+=1
        
#---------------------------------------------------------------------
if __name__=="__main__":
    main()




