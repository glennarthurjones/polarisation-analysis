#!/usr/bin/env python
# Python script to plot some statistics of the H/V results on a 
# month by month basis
import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
from calendar import month_name

#---------------------------------------------------------------------
def flipBaz(azi):
    '''
    Flip the back azimuth values to be between 0-360 
    '''
    baz = azi
    for jj, theta in enumerate(baz):
        if theta < 0:
            baz[jj] = 180 + theta % 180
            
    return baz 

#---------------------------------------------------------------------
def main():
    ''' 
    Main script to plot monthly statistics from the H/V analysis
    '''
    parser = argparse.ArgumentParser(description=
                                    'Plot the monthly H/V results')
    parser.add_argument('--station', type=str,
                    help='station name to process', required=True)
    parser.add_argument('--year', nargs=1,
                    help='year to process', required=True )
    parser.add_argument('--months', nargs='+',
                    help='months to process', required=True)
    parser.add_argument('--code', type=str,
                    help='measurement base code', 
                    required=True)
    parser.add_argument('--locID', nargs='+' ,
                    help='location ID of the station', 
                    required=True)
    parser.add_argument('--dop', type=float,
                    help='DOP filter. Default 0.9', required=False)
   
    # setting up the variables to plot
    args = vars(parser.parse_args())
    station = args['station']
    year = args['year'][0]
    months = args['months'] 
    measurement_code = args['code']
    locID = args['locID']
    if args['dop'] is None:
        dop_thresh = 0.9
    else:
        dop_thresh = args['dop']
    day_sec = 24.*60.*60.

    # sort the months out if necessary
    if len(months) > 1:
        months.sort(key=int)

    # loop over the months
    for kk, month in enumerate(months):
        dirname = "%s/%s.%02d" %(station, year, int(month))
        for loc in locID: 
            # load the relevant summary file
            fnameIn = ('%s/%s_%s_summary.txt'
                    %(dirname, measurement_code, loc))
            try:
                azi, freq, dop, doy, time, hv = np.loadtxt(fnameIn,
                        delimiter=',', comments='#', unpack=True)      
            except Exception as error:
                print('WARNING: %s' %error)
                continue

            freq_list = list(np.unique(freq))
            # compute the period
            T = []
            for f in freq:
	        t=1./f #compute the period
                T.append(t)
 
            # filter the results based on the DOP threshold
            # convert T, time and hv into numpy arrays
            T = np.array(T)
            dopF = dop[dop >= dop_thresh]
            baz = np.array(azi[dop >= dop_thresh]) 
            bazF = flipBaz(baz)
            freqD = freq[dop >= dop_thresh]
            doyTime = doy + (time/day_sec)
            doyTimeD = doyTime[dop >= dop_thresh]
            
            # set up the plots 
            fig = plt.figure(kk, figsize=(12,9))
            plt.rcParams['xtick.labelsize']=16
            plt.rcParams['ytick.labelsize']=16
            # histogram of the back azimuth
            plt.subplot(121)
            plt.scatter(freqD, doyTimeD, 4, c = bazF, marker=".", 
                    cmap = "jet", edgecolor="face", vmin=0, vmax=360)
            # set up the colorbar
            cb = plt.colorbar()
            cb.set_label("Back azimth ($^\circ$)", size=16)
            cb.set_clim([0, 359.99])
            cb.set_ticks([0, 90, 180, 270, 359.99])
            cb.set_ticklabels(['0','90', '180', '270', '360'])

            plt.xlim(min(freq_list), max(freq_list))
            plt.ylim(min(doy), max(doy))
            plt.xlabel("Frequency (Hz)", size=18)
            plt.ylabel("Day of Year", size=18)
            plt.grid() 

            plt.subplot(122)
            plt.scatter(freqD, doyTimeD, 6, c = dopF, marker=".", 
                    cmap = "jet", edgecolor="face", vmin=dop_thresh, vmax=1)
            # set up the colorbar
            cb = plt.colorbar()
            cb.set_label("DOP (%)", size=16)
            cb.set_clim([dop_thresh, 1])
            
            plt.xlim(min(freq_list), max(freq_list))
            plt.ylim(min(doy), max(doy))
            plt.xlabel("Frequency (Hz)", size=18)
            plt.ylabel("Day of Year", size=18)
            plt.grid() 

            mn=month_name[int(month)]
            fig.suptitle("%s %s DOP = %s" %(mn, year, dop_thresh), size=22)
            plt.subplots_adjust(wspace=0.35)

    plt.show()

#---------------------------------------------------------------------
if __name__=="__main__":
    main()

