#!/usr/bin/env python 
# Python script to cleanup any HV sac analysis files
#
import os 
import sys
import glob
from calendar import monthrange

sf = raw_input("Please provide a station names to be processed: ")
station_folder = set(sf.split())

y = raw_input("Please provide years to process: ")
years = set(y.split())

for yr in years:
    try:
        year = int(yr)
    except ValueError:
        print "years is not an integer number. Please provide an appropriate year"
        sys.exit()

monthsInp = raw_input("Please provide a list of the months to be processed: ")
mr = set(monthsInp.split())
month_range = [int(x) for x in mr]
for m in mr:
    try:
        m = int(m)
        if m < 1 or m > 12:
            print "Months must have values between 1 and 12"
            print "Current value %s will be removed" % m
            month_range.remove(m)
    except ValueError:
        print "Months is not a valid number. Please provide a correct month"
        sys.exit()

#station_folder = ["DY2G","NEEM","NUUK"]
#station_folder = ["DBG"]
#years = ["2012"]
#month_range=[3]
# code name of file to be deleted

extensions = ['.asc', '.cmd','.log']
for data_folder in station_folder:
    print '---- Station %s ----' % data_folder
    for year in years:  
#        for m in range(1,4):   #12):
        for m in month_range:
	    month = m
	    ndays = monthrange(int(year), int(month))[1]
	    for d in range(1,ndays+1):
            #for d in range(1,20):
	        day_folder = '%s/%s.%02d/%02d/' %(data_folder, year, month, d)
	        for ext in extensions:   
                    output_file = day_folder + '*' + ext 
                    filelist = glob.glob(output_file)
	            if filelist:
	                print "--> removing H/V %s file for %s-%02d-%02d\n" \
                                %(ext, year, month, d)
		        for fl in filelist:
		            os.remove(fl)


