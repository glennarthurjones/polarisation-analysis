#!/usr/bin/env python 
# Python script to perform the H/V analysis 
import os
import sys
import glob
import argparse
from calendar import monthrange
from datetime import date
from shutil import rmtree
from string import ascii_letters, digits
from random import choice
from multiprocessing import Pool
from tracePreProc import nextpow2
#---------------------------------------------------------------------
def readEx(fname, baseDir):
    '''
    Read the file with the dates we wish to exclude
    '''
    fnameEx = "%s/%s" %(baseDir, fname)
    exList = []
    # check to see if the exclude file exists
    if os.path.isfile(fnameEx):
        with open(fnameEx) as fl:
            exList = []
            for line in fl:
                line = line.strip()
                exList.append(line)

#    filelist = glob.glob(fnameEx)[0]
#    if not filelist:
#        exList = None
#    else:
#        with open(filelist) as fl:
#            exList = []
#            for line in fp:
#                exList.append(line)
    
    return exList
#---------------------------------------------------------------------
def checkTimes(f, exList):
    '''
    Read the sac data files and check to make sure the data is not in
    the list of excluded times. The data are trimmed accordingly and 
    then checked to make sure we have enough left over traces
    '''
    # load the traces and get the number of points
    st = read('Z.sac')
    st += read('E.sac')
    st +=read('N.sac') 
    nptsO = st[0].stats.nsamples
    nptNP2 = nextpow2(npts0)
    # trim the data and see if there has been any change between before and 
    # after
    for line in exList:
        stC = st.copy()
        st = date(line[0])
        et = date(line[1])
        stC.trim(st,et)
        nptsTr = st[0].stats.nsamples
        if nptsTr != npts0:
    	    print "---------------"
            nptNP2tr = nextpow2(nptsTr)
            if nptNP2tr == nptNP2:
                # save the trimmed data as Z, N and E sac files
                for tr in stC:
                    chan = tr.stats.channel
                    tr.data = tr.data.astype(np.float32)
                    fout = "%s.sac" %chan
                    tr.write(fout, format='SAC')
                    print "%s was re-cut to remove bad data" %(f.strip('.sac'))
            else:
                print("%s won't be processed due to time of bad data" 
                    %(f.strip('.sac')))
#---------------------------------------------------------------------

def make_tmp():
    '''
    Generates a temporary file    
    '''
    out = 'tmp' + ''.join([choice(ascii_letters + digits) for n in xrange(5)]) 
    return out  
   
#---------------------------------------------------------------------
def initialize_logging(out, measurement_code, loc):
    '''
    Initialize output logging
    '''
    l = open(out + "/HV." + measurement_code + "." + loc + ".log", "a")
    l_err =  open(out + "/HV." + measurement_code + 
            "." + loc + ".error.log", "a")
    sys.stdout = l
    sys.stderr = l_err
    
    return l, l_err

def close_logging(l, l_err):
    '''
    Close the log file and make reset the stderr and stdout
    '''
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__
    l.close()
    l_err.close()

#---------------------------------------------------------------------
def file_cleanup(tmpfile, out_folder, output_file, measurement_code, loc):
    ''' 
    File cleanup post processing

    Input: 
        tmpfile             : temp file directory
        out_folder          : directory where the output will be stored
        output_file         : output file name
        measurement_code    : measurement code
        loc                 : location code
    '''
    output_polfre = "%s../polfre.%s.%s.log" %(out_folder, measurement_code, loc)
    if os.path.isfile(tmpfile + "/output_file.asc"):
        # if the output file is empty we delete it
        if os.stat(tmpfile + "/output_file.asc").st_size == 0: 
            os.remove(tmpfile + "/output_file.asc")
        else:
            # move and remove the output files
            os.system("cp " + tmpfile + "/output_file.asc " + output_file)
            os.remove(tmpfile + "/output_file.asc")
   
    if os.path.isfile(tmpfile + "/polfre.log"):
        os.system("cp " + tmpfile + "/polfre.log " + output_polfre)
        os.remove(tmpfile + "/polfre.log")
    
    # move the initialized files 
    for tf in glob.glob(tmpfile + "/*.log"):
        # check the size of the files and delete if empty
        if os.stat(tf).st_size == 0:
            os.remove(tf)
        else:
            os.system("cp " + tf +" " + out_folder + "../.")

#---------------------------------------------------------------------
def day_process(base_dir, year, month, d, measurement_code, locList=[]):
    '''
    Processing of H/V data in a folder. Assumes data were pre-processed using
    preprocessingGLISN.py command

    Inputs:
        basedir           : folder base directory (typically station/year/month/)
        year              : year to process 
        month             : month to process
        day               : folder to be processed (typically day)
        measurement_code  : measurement base code for output
        locList           : list of station locations. Default = []
    '''
    cwd = os.getcwd() # get current directory
    day_folder = '%s/%s/processed/' %(base_dir,  d)
    # check to see if we have any files in the current directory
    filelist = glob.glob(day_folder + "/*BHZ*chunk*sac")
    if not filelist:
        # generate an error output log file 
        errorMsg = 'Error: Preprocessed files do not exist' % filelist
        f = open(base_dir + '/' + d +  '/HV.' + measurement_code +
                ".9999.error.log", "a+") 
        f.write(errorMsg)
        f.close()
        return 
               
    # get the location of the different trace if locList is undefined

    if not locList:
        for fl in filelist: 
            fl = os.path.basename(fl)
            [net, station, loc, comp, ext] =fl.split(".")
            comp = comp.split("_")[0]
            if loc not in locList:
                locList.append(loc)
    else:
        fl = filelist[0]
        fl = os.path.basename(fl)
        [net, station, loc, comp, ext] =fl.split(".")
        comp = comp.split("_")[0]

    # I am only interested in those stations which have a  N, E, Z orientation.
    # I will deal with the the 1, 2, Z naming convention once I've spoken to 
    # Ana about this loop over the location files
    for loc in locList: 
        fname = ("%s%s.%s.%s.BHZ_chunk*sac" \
                    %(day_folder, net, station, loc))
        filelist = glob.glob(fname) 
        # sort the filelist
        filelistS = sorted(filelist, 
            key=lambda x: int(x.split('_')[-1].split('sps')[0]))
        # set the output file based on chunk iteration
        output_file = ('%s../%s.%02d.%s.%s.%s.asc' \
    		%(day_folder, year, int(month), d, measurement_code, loc))
        
        # make a temporary file to perform the processing
        tmpfile = make_tmp()
        os.mkdir(tmpfile)
        # initialize output logging 
        log, logErr = initialize_logging(tmpfile, measurement_code, loc)
        for f in filelistS:
            Zfile = f
            Nfile = f.replace("BHZ", "BHN")
            Efile = f.replace("BHZ", "BHE")
                            
            # make sure these files exist
            if (os.path.isfile(Zfile) and os.path.isfile(Nfile) 
                        and os.path.isfile(Efile)):
                print Zfile
    		print Nfile
    		print Efile
    		print "---------------"
                # check to make sure the current trace falls in the list
                # of dates we are excluding
    		os.system("cp " + Zfile + " " + tmpfile + "/Z.sac")
    		os.system("cp " + Nfile + " " + tmpfile + "/N.sac")
    		os.system("cp " + Efile + " " + tmpfile + "/E.sac")
                os.system("cp run_polfre.cmd " + tmpfile + "/.")
                # we must be in the working directory to get the code to 
                # work. I think it's a SAC thing since the file is being
                # identified for an input - I can't see this being overly
                # efficient having to change directories...
                os.chdir(tmpfile)
                
    		# run the bash script with the variables to compute the H/V 
    	        os.system("sh ./run_polfre.cmd")
                os.chdir(cwd) 
                
        # file cleanup  
        close_logging(log, logErr)  
        file_cleanup(tmpfile, day_folder, output_file, measurement_code, loc)

        # cleanup the tmp folder and any other files which have not been moved
        try:
            rmtree(tmpfile)
        except OSError as e:
            # print to standard error file
            f = open(day_folder + '../' + measurement_code +"_error.log", "a+")
            f.write("Error: %s - %s." % (e.filename, e.strerror))
            f.close()

    return
#---------------------------------------------------------------------
def day_wrap(args):
    '''
    Wrapper function for use with the multiprocessing pool command which
    unpacks the list of inputs and then calls day_process
    '''
    return day_process(*args)

#---------------------------------------------------------------------
def main():
    '''
    Script to perform the H/V analysis on data which have been pre-processed
    using the preprocessingGLISN.py command
    '''
    parser = argparse.ArgumentParser(description=
                        "Compute the polarization attributed of data",
                        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--station', nargs='+',
                    help="station name to process", required=True)
    parser.add_argument('--years', nargs='+',
                    help="years to process", required=True )
    parser.add_argument('--months', nargs='+',
                    help="months to process", required=True)
    parser.add_argument('--code', nargs=1, type=str,
                    help="measurement base code for output", required=True)
    parser.add_argument('--nodes', nargs=1, type=int,
                    help="number of processors to be used in the processing\n" 
                    "Default = 2", required=False)
    parser.add_argument('--exclude', nargs=1, type=str,
                    help="filename of the days and times we wish to exclude\n"
                    "The file should consist of single column file with the\n"
                    "days of the month we wish to exclude.",
                    required=False)
    parser.add_argument('--locList', nargs='+',
                    help="list of location ID to be processed. The location\n"
                    "ID must have sac files stored as Z,N,E. Default is to \n"
                    "process all location",
                    required=False)
    #read the aruments
    args=vars(parser.parse_args())
    station_folder = args['station']
    years = args['years']
    month_range = args['months']
    measurement_code = args['code'][0]
    if args['nodes'] is None:
        pool_process = 2 # number of pool processors
    else:
        pool_process = args['nodes'][0]
   
    if args['exclude'] is None:
        exclude = False
    else:
        exclude = args['exclude'][0]
   
    if args['locList'] is None:
        locList = []
    else:
        locList = args['locList']

    # looping over all of the station to get the correct base directory
    for jj, data_folder in enumerate(station_folder):
        print '---- Processing  station %s ----\n' % data_folder
        dirList = os.listdir(data_folder)
        for year in years:
            for month in month_range:
                print "--> Processing %s %02d" %(year, int(month))
                base_dir = data_folder + "/%s.%02d" %(year, int(month))
                nDays = monthrange(int(year), int(month))[1]
                daysM = ['%02d' % (jj+1) for jj in range(0,nDays)]
                # read the exclude time
                if exclude:
                    exDays = readEx(exclude, base_dir)
                    days = sorted(list(set(daysM).difference(exDays)))
                else:
                    days = daysM
#                days = ['04']
                pool_args = []         
                for d in sorted(days):
                    pool_args.append((base_dir, year, month, d, 
                        measurement_code, locList))

                p = Pool(pool_process)
                p.map(day_wrap, pool_args)
#---------------------------------------------------------------------
if __name__=="__main__":
    main()

